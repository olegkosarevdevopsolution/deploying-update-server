<?php

namespace App\Abstracts;

use App\Controllers\BaseController;
use App\Interface\UpdateRouteHandlerInterface;

/**
 * UpdateRouteHandlerAbstract
 *
 * description
 *
 * @package  \App\Abstracts;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/Deploying%20Update%20Server
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
abstract class UpdateRouteHandlerAbstract extends BaseController implements UpdateRouteHandlerInterface
{
    private string $prefix;
    private string $name;

    // abstract public function handle(string $name): string;

    /**
     * Set the clean name by replacing a substring.
     *
     * @param string $search
     * @param string $subject
     * @param string $replace
     * @return self
     */
    protected function setCleanName(string $search, string $subject, string $replace = ''): self
    {
        $this->name = str_replace($search, $replace, $subject);
        return $this;
    }

    /**
     * Retrieve the clean name.
     *
     * @return string
     */
    protected function getCleanName(): string
    {
        return $this->name;
    }

    /**
     * Concatenates the prefix with the clean name.
     *
     * @return string
     */
    protected function getCleanNameWithPrefix(): string
    {
        return $this->getPrefix() . $this->getCleanName();
    }

    /**
     * Set the prefix.
     *
     * @param string $prefix
     * @return self
     */
    protected function setPrefix(string $prefix): self
    {
        $this->prefix = $prefix;
        return $this;
    }

    /**
     * GET the prefix.
     *
     * @return string
     */
    protected function getPrefix(): string
    {
        return $this->prefix;
    }
}
