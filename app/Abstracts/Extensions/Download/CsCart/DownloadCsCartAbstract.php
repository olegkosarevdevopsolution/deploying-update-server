<?php

namespace App\Abstracts\Extensions\Download\CsCart;

use Config\Services;
use App\Abstracts\Extensions\Download\DownloadAbstract;

/**
 * DownloadCsCartAbstract
 *
 * This class represents the abstract download functionality for CsCart extensions.
 *
 * @package  \App\Abstracts\Extensions\Download\CsCart;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2024 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/Deploying%20Update%20Server
 * @version 1.0.0
 * @since 1.0.0
 */
abstract class DownloadCsCartAbstract extends DownloadAbstract
{
    /**
     * The base patch. The path where the extension will be downloaded.
     * @var string
     */
    protected string $basePatch = WRITEPATH . 'Update/CsCart/CsCart';

    /**
     * The CsCart4DownloadLinksService instance.
     * @var \App\Services\Update\CsCart\CsCart4\CsCart4DownloadLinksService
     */
    private \App\Services\Update\CsCart\CsCart4\CsCart4DownloadLinksService $csCart4DownloadLinkService;

    /**
     * The full path of the extension.
     *
     * @var string
     */
    public string $fullPatch;

    /**
     * Constructor for DownloadCsCartAbstract class.
     */
    public function __construct()
    {
        $this->csCart4DownloadLinkService = Services::csCart4DownloadLinkService();
    }

    /**
     * Get the file path based on CsCart version, name, version, file name, and file format.
     *
     * @param string $csCartVersion The version of CsCart.
     * @param string $name The name of the extension.
     * @param string $version The version of the extension.
     * @param string $fileName The file name of the extension.
     * @param string $fileFormat The file format of the extension.
     * @return string|null The file path or null if not found.
     */
    protected function getFilePath(string $csCartVersion, string $name, string $version, string $fileName, string $fileFormat): ?string
    {
        // Check the CsCart version
        switch ($csCartVersion) {
                // For CsCart version 4
            case '4':
                // Get the download link by file name
                $downloadLink = $this->csCart4DownloadLinkService->getDownloadUrlLinkByFileName($fileName);

                // If download link not found, return null
                if (!$downloadLink) {
                    return null;
                }

                // Get the file format from the download link
                $downloadedFileFormat = $downloadLink["format"];

                // If the file format does not match, return null
                if ($downloadedFileFormat != $fileFormat) {
                    return null;
                }

                // Return the full patch
                return $this->fullPatch;

                // For other CsCart versions, return null
            default:
                return null;
        }
    }
}
