<?php

namespace App\Abstracts\Extensions\Download\Joomla;

use App\Abstracts\Extensions\Download\DownloadAbstract;
use Config\Services;
use SimpleXMLElement;

/**
 * DownloadJoomlaAbstract
 *
 * This class represents an abstract for downloading Joomla extensions.
 *
 * @package  \App\Abstracts\Extensions\Download\Joomla;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2024 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/Deploying%20Update%20Server
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
abstract class DownloadJoomlaAbstract extends DownloadAbstract
{
    /**
     * The base path for the Joomla extension files.
     *
     * @var string
     */
    protected string $basePatch = WRITEPATH . 'Update/Joomla/Joomla';

    /**
     * The Joomla 3 download link service.
     *
     * @var \App\Services\Update\Joomla\Joomla3\Joomla3DownloadLinkService
     */
    private \App\Services\Update\Joomla\Joomla3\Joomla3DownloadLinkService $joomla3DownloadLinkService;

    /**
     * The Joomla 4 download link service.
     *
     * @var \App\Services\Update\Joomla\Joomla4\Joomla4DownloadLinkService
     */
    private \App\Services\Update\Joomla\Joomla4\Joomla4DownloadLinkService $joomla4DownloadLinkService;

    /**
     * The Joomla 5 download link service.
     *
     * @var \App\Services\Update\Joomla\Joomla5\Joomla5DownloadLinkService
     */
    private \App\Services\Update\Joomla\Joomla5\Joomla5DownloadLinkService $joomla5DownloadLinkService;

    /**
     * The full path of the downloaded file.
     *
     * @var string
     */
    public string $fullPatch;

    /**
     * Constructor for DownloadJoomlaAbstract class.
     */
    public function __construct()
    {
        $this->joomla3DownloadLinkService = Services::joomla3DownloadLinkService();
        $this->joomla4DownloadLinkService = Services::joomla4DownloadLinkService();
        $this->joomla5DownloadLinkService = Services::joomla5DownloadLinkService();
    }

    /**
     * Get the file path for the downloaded Joomla extension file.
     *
     * @param string $joomlaVersion The version of Joomla.
     * @param string $name The name of the extension.
     * @param string $version The version of the extension.
     * @param string $fileName The file name of the extension.
     * @param string $fileFormat The format of the file.
     * @return string|null The file path if valid, null otherwise.
     */
    protected function getFilePath(string $joomlaVersion, string $name, string $version, string $fileName, string $fileFormat): ?string
    {
        // Check the Joomla version
        switch ($joomlaVersion) {
            case '3':
                // Get the download link by file name for Joomla 3
                $downloadLink = $this->joomla3DownloadLinkService->getDownloadUrlLinkByFileName($fileName);
                // Return null if no download link is found
                if (!$downloadLink) {
                    return null;
                }
                // Get the file format from the download link
                $downloadedFormat = $downloadLink["format"];
                // Return null if the downloaded format does not match the required format
                if ($downloadedFormat != $fileFormat) {
                    return null;
                }
                // Return the file path if all checks pass
                return $this->fullPatch;

            case '4':
                // Get the download link by file name for Joomla 4
                $downloadLink = $this->joomla4DownloadLinkService->getDownloadUrlLinkByFileName($fileName);
                // Return null if no download link is found
                if (!$downloadLink) {
                    return null;
                }
                // Get the file format from the download link
                $downloadedFormat = $downloadLink["format"];
                // Return null if the downloaded format does not match the required format
                if ($downloadedFormat != $fileFormat) {
                    return null;
                }
                // Return the file path if all checks pass
                return $this->fullPatch;

            case '5':
                // Get the download link by file name for Joomla 5
                $downloadLink = $this->joomla4DownloadLinkService->getDownloadUrlLinkByFileName($fileName);
                // Return null if no download link is found
                if (!$downloadLink) {
                    return null;
                }
                // Get the file format from the download link
                $downloadedFormat = $downloadLink["format"];
                // Return null if the downloaded format does not match the required format
                if ($downloadedFormat != $fileFormat) {
                    return null;
                }
                // Return the file path if all checks pass
                return $this->fullPatch;

            default:
                // Return null for unsupported Joomla versions
                return null;
        }
    }
}
