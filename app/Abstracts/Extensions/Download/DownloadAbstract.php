<?php

namespace App\Abstracts\Extensions\Download;

use App\Controllers\BaseController;
use SimpleXMLElement;

/**
 * DownloadAbstract
 *
 * This abstract class provides a template for downloading files related extensions or addons.
 *
 * @package  \App\Controllers\Extensions\Download;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2024 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/Deploying%20Update%20Server
 * @version 1.0.0
 * @since 1.0.0
 */
abstract class DownloadAbstract extends BaseController
{
    /**
     * Define an abstract method that returns the file path based on the Joomla version, name, version, file name, and file format.
     *
     * @param string $joomlaVersion The version of Joomla
     * @param string $name The name of the extension
     * @param string $version The version of the extension
     * @param string $fileName The name of the file
     * @param string $fileFormat The format of the file
     * @return string The file path
     */
    abstract protected function getFilePath(string $joomlaVersion, string $name, string $version, string $fileName, string $fileFormat): ?string;

    /**
     * Define a common method that handles the download request.
     *
     * @param string $joomlaVersion The version of Joomla
     * @param string $name The name of the extension
     * @param string $version The version of the extension
     * @param string $fileName The name of the file
     * @param string $fileFormat The format of the file
     * @return  \CodeIgniter\HTTP\ResponseInterface The response body
     */
    public function downloadFile(string $joomlaVersion, string $name, string $version, string $fileName, string $fileFormat): \CodeIgniter\HTTP\ResponseInterface
    {
        // Get the file path using the abstract method
        $filePath = $this->getFilePath($joomlaVersion, $name, $version, $fileName, $fileFormat);
        // Check if the file exists
        if (!file_exists($filePath)) {
            $xml = new SimpleXMLElement('<Error></Error>');
            $xml->addChild("Code", "NoSuchKey");
            $xml->addChild("Message", "The specified key does not exist.");
            $xml->addChild("Key", "downloads/$fileName/$fileFormat");
            $xml->addChild("RequestId", hash("sha256", $filePath));
            $xml->addChild("HostId", hash("sha256", $this->request->getIPAddress() . $filePath));
            $this->response->setStatusCode(404);
            $this->response->setHeader('Content-Type', 'text/xml');
            // Output the XML document to the browser
            return $this->response->setBody($xml->asXML());
        }

        // Get the file size and type
        $fileSize = filesize($filePath);
        $fileType = mime_content_type($filePath);

        // Set the status code to 200 (OK)
        $this->response->setStatusCode(200);

        // Set headers with the file size and type
        $this->response->setHeader('File Size', $fileSize);
        $this->response->setContentType($fileType);

        // Use the $this->response->download method to initiate the file download
        // The first argument is the file path, 
        // and the second argument (null in this case) is the new name of the downloaded file
        // If you pass null, the file will retain its original name
        return $this->response->download($filePath, null);
    }
}
