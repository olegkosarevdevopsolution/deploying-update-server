<?php

namespace App\Database\Migrations\Joomla3;

use CodeIgniter\Database\Migration;

class Joomla3TargetPlatforms extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'extension_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'default' => 'joomla'
            ],
            'version' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'default' => '.*'
            ],
            'min_dev_level' => [
                'type' => 'INT',
                'constraint' => 11,
                'null' => true
            ],
            'max_dev_level' => [
                'type' => 'INT',
                'constraint' => 11,
                'null' => true
            ]
        ]);
        $this->forge->addKey('id', TRUE);
        $this->forge->addForeignKey('extension_id', 'joomla3_extensions_info', 'id', 'CASCADE', 'CASCADE');
        $this->forge->createTable('joomla3_target_platforms');
    }

    public function down()
    {
        $this->forge->dropTable('joomla3_target_platforms');
    }
}
