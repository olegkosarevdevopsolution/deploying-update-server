<?php

namespace App\Database\Migrations\Joomla3;

use CodeIgniter\Database\Migration;

class Joomla3ExtensionsInfo extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'description' => [
                'type' => 'TEXT',
                'null' => true
            ],
            'element' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'type' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'folder' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true
            ],
            'client' => [
                'type' => 'ENUM("0", "1")',
                'default' => '0'
            ],
            'version' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'infourl' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true
            ],
            'joomla_tag_name' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true
            ],
            'changelogurl' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'maintainer' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'maintainerurl' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'php_minimum_version' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'created_at' => [
                'type' => 'DATETIME'
            ],
            'updated_at' => [
                'type' => 'DATETIME'
            ]
        ]);
        $this->forge->addKey('id', TRUE);
        $this->forge->addForeignKey('php_minimum_version', 'php_versions', 'version', 'CASCADE', 'CASCADE');
        $this->forge->addForeignKey('joomla_tag_name', 'joomla_tags', 'name', 'CASCADE', 'CASCADE');
        $this->forge->createTable('joomla3_extensions_info');
    }

    public function down()
    {
        $this->forge->dropTable('joomla3_extensions_info');
    }
}
