<?php

namespace App\Database\Migrations\CsCart4;

use CodeIgniter\Database\Migration;

class CsCart4AddonsInfo extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'element' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'description' => [
                'type' => 'TEXT',
                'null' => true
            ],
            'from_version' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'to_version' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'timestamp' => [
                'type' => 'BIGINT',
            ],
            'size' => [
                'type' => 'BIGINT',
            ],
            'created_at' => [
                'type' => 'DATETIME'
            ],
            'updated_at' => [
                'type' => 'DATETIME'
            ]
        ]);
        $this->forge->addKey('id', TRUE);
        $this->forge->createTable('cscart4_addons_info');
    }

    public function down()
    {
        $this->forge->dropTable('cscart4_addons_info');
    }
}
