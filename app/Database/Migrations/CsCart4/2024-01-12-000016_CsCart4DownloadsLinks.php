<?php

namespace App\Database\Migrations\CsCart4;

use CodeIgniter\Database\Migration;

class CsCart4DownloadsLinks extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'addon_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
            ],
            'downloadurl' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true
            ],
            'format' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'default' => 'zip',
            ]
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addForeignKey('addon_id', 'cscart4_addons_info', 'id', 'CASCADE', 'CASCADE');
        $this->forge->createTable('cscart4_download_links');
    }

    public function down()
    {
        $this->forge->dropTable('cscart4_download_links');
    }
}
