<?php

namespace App\Database\Migrations\Joomla4;

use CodeIgniter\Database\Migration;

class Joomla4ExtensionMappingSupportedDatabases extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'ext_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
            ],
            'sd_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
            ],
        ]);

        $this->forge->addKey('id', true);
        $this->forge->addForeignKey('ext_id', 'joomla4_extensions_info', 'id');
        $this->forge->addForeignKey('sd_id', 'supported_databases_versions', 'id');
        $this->forge->createTable('jomla4_extension_mapping_supported_databases');
    }

    public function down()
    {
        $this->forge->dropTable('jomla4_extension_mapping_supported_databases');
    }
}
