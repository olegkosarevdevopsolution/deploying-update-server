<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class JoomlaTags extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
        ]);
        $this->forge->addKey('id', TRUE);
        $this->forge->addKey("name");
        $this->forge->createTable('joomla_tags');
    }

    public function down()
    {
        $this->forge->dropTable('joomla_tags');
    }
}
