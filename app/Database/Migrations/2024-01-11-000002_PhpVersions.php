<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class PhpVersions extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true
            ],
            'version' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ]
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addKey("version");
        $this->forge->createTable('php_versions');
    }

    public function down()
    {
        $this->forge->dropTable('php_versions');
    }
}
