<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class SupportedDatabasesVersions extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'version' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ]
        ]);
        $this->forge->addKey('id', TRUE);
        $this->forge->createTable('supported_databases_versions');
    }

    public function down()
    {
        $this->forge->dropTable('supported_databases_versions');
    }
}
