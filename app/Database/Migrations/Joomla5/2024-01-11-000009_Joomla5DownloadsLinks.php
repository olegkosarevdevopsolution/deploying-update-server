<?php

namespace App\Database\Migrations\Joomla5;

use CodeIgniter\Database\Migration;

class Joomla5DownloadsLinks extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'extension_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
            ],
            'downloadurl' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true
            ],
            'downloadsource' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true
            ],
            'type' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'default' => 'full',
            ],
            'format' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'default' => 'zip',
            ]
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addForeignKey('extension_id', 'joomla5_extensions_info', 'id', 'CASCADE', 'CASCADE');
        $this->forge->createTable('joomla5_download_links');
    }

    public function down()
    {
        $this->forge->dropTable('joomla5_download_links');
    }
}
