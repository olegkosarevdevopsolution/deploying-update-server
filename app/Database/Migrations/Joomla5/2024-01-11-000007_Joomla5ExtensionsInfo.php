<?php

namespace App\Database\Migrations\Joomla5;

use CodeIgniter\Database\Migration;

class Joomla5ExtensionsInfo extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'description' => [
                'type' => 'TEXT',
                'null' => true
            ],
            'element' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'type' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'folder' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true
            ],
            'client' => [
                'type' => 'ENUM("site", "administrator")',
                'default' => 'site'
            ],
            'version' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'infourl' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true
            ],
            'joomla_tag_name' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true
            ],
            'changelogurl' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true
            ],
            'maintainer' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'maintainerurl' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'php_minimum_version' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'sha256' => [
                'type' => 'VARCHAR',
                'constraint' => 64,
                'null' => true
            ],
            'sha384' => [
                'type' => 'VARCHAR',
                'constraint' => 96,
                'null' => true
            ],
            'sha512' => [
                'type' => 'VARCHAR',
                'constraint' => 128,
                'null' => true
            ],
            'created_at' => [
                'type' => 'DATETIME'
            ],
            'updated_at' => [
                'type' => 'DATETIME'
            ]
        ]);
        $this->forge->addKey('id', TRUE);
        $this->forge->addForeignKey('php_minimum_version', 'php_versions', 'version', 'CASCADE', 'CASCADE');
        $this->forge->addForeignKey('joomla_tag_name', 'joomla_tags', 'name', 'CASCADE', 'CASCADE');
        $this->forge->createTable('joomla5_extensions_info');
    }

    public function down()
    {
        $this->forge->dropTable('joomla5_extensions_info');
    }
}
