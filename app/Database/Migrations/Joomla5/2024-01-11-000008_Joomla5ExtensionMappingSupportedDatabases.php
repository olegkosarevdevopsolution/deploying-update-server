<?php

namespace App\Database\Migrations\Joomla5;

use CodeIgniter\Database\Migration;

class Joomla5ExtensionMappingSupportedDatabases extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'ext_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
            ],
            'sd_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
            ],
        ]);

        $this->forge->addKey('id', true);
        $this->forge->addForeignKey('ext_id', 'joomla5_extensions_info', 'id');
        $this->forge->addForeignKey('sd_id', 'supported_databases_versions', 'id');
        $this->forge->createTable('jomla5_extension_mapping_supported_databases');
    }

    public function down()
    {
        $this->forge->dropTable('jomla5_extension_mapping_supported_databases');
    }
}
