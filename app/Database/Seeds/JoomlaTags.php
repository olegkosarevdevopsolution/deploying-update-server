<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class JoomlaTags extends Seeder
{
    public function run()
    {
        $data = [
            [
                'name' => 'dev'
            ],
            [
                'name' => 'alpha'
            ],
            [
                'name' => 'beta'
            ],
            [
                'name' => 'rc'
            ],
            [
                'name' => 'stable'
            ]
        ];

        $this->db->table('joomla_tags')->insertBatch($data);
    }
}
