<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class PhpVersions extends Seeder
{
    public function run()
    {
        $data = [
            [
                "version" => "5.1"
            ],
            [
                "version" => "5.2"
            ],
            [
                "version" => "5.3"
            ],
            [
                "version" => "5.4"
            ],
            [
                "version" => "5.5"
            ],
            [
                "version" => "5.6"
            ],
            [
                "version" => "7.0"
            ],
            [
                "version" => "7.1"
            ],
            [
                "version" => "7.2"
            ],
            [
                "version" => "7.3"
            ],
            [
                "version" => "7.4"
            ],
            [
                "version" => "8.0"
            ],
            [
                "version" => "8.1"
            ],
            [
                "version" => "8.2"
            ],
        ];

        $this->db->table('php_versions')->insertBatch($data);
    }
}
