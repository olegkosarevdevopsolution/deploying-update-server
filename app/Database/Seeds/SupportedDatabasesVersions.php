<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class SupportedDatabasesVersions extends Seeder
{
    public function run()
    {
        $mariadb = [
            "name" => "mariadb",
            "version" => [
                "5.1",
                "5.2",
                "5.3",
                "5.5",
                "10.0",
                "10.1",
                "10.2",
                "10.3",
                "10.4",
                "10.5",
                "10.6",
                "10.7",
                "10.8",
                "10.9",
                "10.10",
                "10.11",
                "11.0",
                "11.1",
                "11.2",
                "11.3",
                "11.4"
            ]
        ];

        $mysql = [
            "name" => "mysql",
            "version" => [
                '5.1',
                '5.5',
                '5.6',
                '5.7',
                '8.0',
                '8.1',
                '8.2'
            ]
        ];
        $postgresql = [
            "name" => "postgresql",
            "version" => [
                '6.0',
                '6.1',
                '6.2',
                '6.3',
                '6.4',
                '6.5',
                '7.0',
                '7.1',
                '7.2',
                '7.3',
                '7.4',
                '8.0',
                '8.1',
                '8.2',
                '8.3',
                '8.4',
                '9.0',
                '9.1',
                '9.2',
                '9.3',
                '9.4',
                '9.5',
                '9.6',
                '10	',
                '11	',
                '12	',
                '13	',
                '14	',
                '15	',
                '16',
            ]
        ];
        $mssql = [
            'name' => 'mssql',
            'version' => [
                '16.0.1000.6',
                '15.0.2000.5',
                '14.0.1000.169',
                '13.0.1601.5',
                '12.0.2000.8',
                '11.0.2100.60',
                '10.50.1600.1',
                '10.0.1600.22',
                '9.0.1399.06',
                '8.0.194',
                '7.0.623',
                '6.00.121',
            ]
        ];
        $data = [$mariadb, $mysql, $postgresql, $mssql];

        foreach ($data as $database) {
            $name = $database['name'];
            $versions = $database['version'];

            foreach ($versions as $version) {
                $this->db->table('supported_databases_versions')->insert([
                    'name' => $name,
                    'version' => $version
                ]);
            }
        }
    }
}
