<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

/**
 * Joomla4ExtensionsInfo
 *
 * description
 *
 * @package \App\Database\Seeds;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2024 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/Deploying%20Update%20Server
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class Joomla4ExtensionsInfo extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'Datamarket Joomla Template 1.0.0  for Joomla, version 4.X',
                'description' => 'The Datamarket Joomla template 1.0.0 offers a modern and sleek design tailored for data-driven websites. With its responsive layout and customizable features, the template provides a professional and user-friendly experience for showcasing data. Compatible with Joomla version 4.X and later',
                'element' => 'tpl_datamarket',
                'type' => 'template',
                'folder' => null,
                'client' => 'site',
                'version' => '1.0.0',
                'infourl' => 'https://update.idata.lv/extensions/templates/datamarket.html',
                'joomla_tag_name' => 5,
                'changelogurl' =>  'https://update.idata.lv/extensions/templates/datamarket/changelog.html',
                'maintainer' => 'Oleg Kosarev <dev.oleg.kosarev@outlook.com>',
                'maintainerurl' => 'https://dev.azure.com/OlegKosarevDevOpsolution/',
                'php_minimum_version' => '8.0',
                'sha256' => null,
                'sha384' => null,
                'sha512' => null,
            ],
            [
                'id' => '2',
                'name' => 'The Live Chat Bitrix24 module for Joomla, version 4.X',
                'description' => 'Module Live Chat Bitrix24 for Joomla, providing seamless integration of Bitrix24\'s live chat functionality into your Joomla website. This version of the module is compatible with Joomla version 4.X.',
                'element' => 'mod_bitrix24',
                'type' => 'module',
                'folder' => null,
                'client' => 'site',
                'version' => '1.0.0',
                'infourl' => 'https://update.idata.lv/extensions/module/bitrix24.html',
                'joomla_tag_name' => 5,
                'changelogurl' => 'https://update.idata.lv/extensions/module/bitrix24/changelog.html',
                'maintainer' => 'Oleg Kosarev <dev.oleg.kosarev@outlook.com>',
                'maintainerurl' => 'https://dev.azure.com/OlegKosarevDevOpsolution/',
                'php_minimum_version' => '8.0',
                'sha256' => null,
                'sha384' => null,
                'sha512' => null,
            ]
        ];
        $this->db->table('joomla4_extensions_info')->insertBatch($data);
    }
}
