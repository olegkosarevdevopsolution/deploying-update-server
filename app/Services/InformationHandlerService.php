<?php

namespace App\Services;

use Exception;
use SimpleXMLElement;
use CodeIgniter\HTTP\Response;

/**
 * InformationHandlerService
 *
 * description
 *
 * @package \App\Services;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2024 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/Deploying%20Update%20Server
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class InformationHandlerService
{
    protected $response;

    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    public function handleMissingInfo(string $infoKey, array $infoValue, string $errorMessage, string $descriptionMessage = null)
    {
        if (array_key_exists($infoKey, $infoValue) == false or empty($infoValue[$infoKey])) {
            $xml = new SimpleXMLElement('<response/>'); // Create a new SimpleXMLElement
            $xml->addChild('error', 'Error in ' . $infoKey . ': ' . $errorMessage);
            if (!empty($descriptionMessage)) {
                $xml->addChild('description', $descriptionMessage);
            }
            return $xml->asXML(); // Return the error message as a SimpleXMLElement
        }
        return null; // Return null when the condition is not met
    }

    /**
     * Handle an exception and return an error message as a SimpleXMLElement.
     *
     * @param Exception $e The exception to handle
     * @return SimpleXMLElement The error message as a SimpleXMLElement
     */
    public function handleException(Exception $e, string $errorMessage = null): SimpleXMLElement
    {
        // Handle the exception and return an error message
        // You can log the exception here if needed
        // error_log('Error: ' . $e->getMessage());
        $errorMessageStr = null;
        if (!empty($errorMessage)) {
            $errorMessageStr = '<error>' . $errorMessage . '</error>';
        }
        return new SimpleXMLElement($errorMessageStr . '<description>' . $e->getMessage() . '</description>');
    }
}
