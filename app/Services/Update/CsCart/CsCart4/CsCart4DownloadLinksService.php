<?php

namespace App\Services\Update\CsCart\CsCart4;

use App\Models\Update\CsCart\CsCart4\CsCart4DownloadLinksModel;

/**
 * CsCart4DownloadLinksService
 *
 * description
 *
 * @package \App\Services\Update\CsCart\CsCart4;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2024 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/Deploying%20Update%20Server
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class CsCart4DownloadLinksService
{
    private $downloadLinksModel;

    public function __construct(CsCart4DownloadLinksModel $downloadLinksModel)
    {
        $this->downloadLinksModel = $downloadLinksModel;
    }

    public function getAllDownloadLinks()
    {
        return $this->downloadLinksModel->getAllDownloadLinks();
    }

    public function getDownloadLinkById(int $id)
    {
        return $this->downloadLinksModel->getDownloadLinkById($id);
    }

    public function getDownloadLinkByAddonId(int $addon_id)
    {
        // Assuming there's a relationship between addons and download links
        // You can implement the logic to retrieve download link by addon ID here
        return $this->downloadLinksModel->getDownloadLinkByAddonId($addon_id);
    }

    public function getDownloadUrlLinkByFileName(string $fileName)
    {
        return $this->downloadLinksModel->getDownloadUrlLinkByFileName($fileName);
    }

    public function createDownloadLink(array $data)
    {
        return $this->downloadLinksModel->createDownloadLink($data);
    }

    public function updateDownloadLink(int $id, array $data)
    {
        return $this->downloadLinksModel->updateDownloadLink($id, $data);
    }

    public function deleteDownloadLink(int $id)
    {
        return $this->downloadLinksModel->deleteDownloadLink($id);
    }
}
