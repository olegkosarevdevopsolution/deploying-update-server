<?php

namespace App\Services\Update\CsCart\CsCart4;

use App\Models\Update\CsCart\CsCart4\CsCart4AddonsInfoModel;

/**
 * CsCart4AddonsInfoService
 *
 * description
 *
 * @package \App\Services\Update\CsCart\CsCart4;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2024 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/Deploying%20Update%20Server
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class CsCart4AddonsInfoService
{
    private $addonsInfoModel;
    private $downloadLinksService;

    public function __construct(CsCart4AddonsInfoModel $addonsInfoModel, CsCart4DownloadLinksService $downloadLinksService)
    {
        $this->addonsInfoModel = $addonsInfoModel;
        $this->downloadLinksService = $downloadLinksService;
    }

    public function getAllAddonsInfo()
    {
        return $this->addonsInfoModel->getAllAddonsInfo();
    }

    public function getAddonInfoById(int $id)
    {
        return $this->addonsInfoModel->getAddonInfoById($id);
    }

    public function getAddonInfoByElement(string $element)
    {
        return $this->addonsInfoModel->getAddonInfoByElement($element);
    }

    public function getLastVersionForElement(string $element)
    {
        $getLastVersionForElement = $this->addonsInfoModel->getLastVersionForElement($element);
        if (count($getLastVersionForElement) <= 0) {
            return [];
        }
        $id = $getLastVersionForElement['id'];
        // Get the download link by addon ID
        $getDownloadLinkByAddonId = $this->downloadLinksService->getDownloadLinkByAddonId($id);
        $getLastVersionForElement['file'] = null;
        if ($getDownloadLinkByAddonId) {
            $getLastVersionForElement['file'] = $getDownloadLinkByAddonId["downloadurl"];
        }
        return $getLastVersionForElement;
    }

    public function createAddonInfo(array $data)
    {
        return $this->addonsInfoModel->createAddonInfo($data);
    }

    public function updateAddonInfo(int $id, array $data)
    {
        return $this->addonsInfoModel->updateAddonInfo($id, $data);
    }

    public function deleteAddonInfo(int $id)
    {
        return $this->addonsInfoModel->deleteAddonInfo($id);
    }
}
