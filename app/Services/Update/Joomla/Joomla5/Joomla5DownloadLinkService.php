<?php

namespace App\Services\Update\Joomla\Joomla5;

use App\Models\Update\Joomla\Joomla5\Joomla5DownloadLinkModel;

/**
 * Joomla5DownloadLinkService
 *
 * description
 *
 * @package \App\Services\Update\Joomla\Joomla5;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2024 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/Deploying%20Update%20Server
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class Joomla5DownloadLinkService
{
    protected $downloadLinkModel;

    public function __construct(Joomla5DownloadLinkModel $downloadLinkModel)
    {
        $this->downloadLinkModel = $downloadLinkModel;
    }

    public function getAllDownloadLinks()
    {
        return $this->downloadLinkModel->getAllDownloadLinks();
    }

    public function getDownloadLinkById($id)
    {
        return $this->downloadLinkModel->getDownloadLinkById($id);
    }

    public function getDownloadLinksByExtensionId($extensionId)
    {
        return $this->downloadLinkModel->getDownloadLinksByExtensionId($extensionId);
    }

    public function getDownloadUrlLinkByFileName($fileName)
    {
        return $this->downloadLinkModel->getDownloadUrlLinkByFileName($fileName);
    }

    public function createDownloadLink($data)
    {
        return $this->downloadLinkModel->createDownloadLink($data);
    }

    public function updateDownloadLink($id, $data)
    {
        return $this->downloadLinkModel->updateDownloadLink($id, $data);
    }

    public function deleteDownloadLink($id)
    {
        return $this->downloadLinkModel->deleteDownloadLink($id);
    }
}
