<?php

namespace App\Services\Update\Joomla\Joomla5;

use App\Models\Update\Joomla\Joomla5\Joomla5TargetPlatformsModel;

/**
 * Joomla5TargetPlatformsService
 *
 * description
 *
 * @package \App\Services\Update\Joomla\Joomla5;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2024 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/Deploying%20Update%20Server
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class Joomla5TargetPlatformsService
{
    protected $joomla5TargetPlatformsModel;

    public function __construct(Joomla5TargetPlatformsModel $joomla5TargetPlatformsModel)
    {
        $this->joomla5TargetPlatformsModel = $joomla5TargetPlatformsModel;
    }

    public function getAllPlatforms()
    {
        return $this->joomla5TargetPlatformsModel->getAll();
    }

    public function getPlatformById($id)
    {
        return $this->joomla5TargetPlatformsModel->getById($id);
    }

    public function getTargetPlatformsByExtensionId($extensionId)
    {
        return $this->joomla5TargetPlatformsModel->getTargetPlatformsByExtensionId($extensionId);
    }

    public function createPlatform($data)
    {
        return $this->joomla5TargetPlatformsModel->insertPlatform($data);
    }

    public function updatePlatform($id, $data)
    {
        return $this->joomla5TargetPlatformsModel->updatePlatform($id, $data);
    }

    public function deletePlatform($id)
    {
        return $this->joomla5TargetPlatformsModel->deletePlatform($id);
    }
}
