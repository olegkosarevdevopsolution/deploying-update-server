<?php

namespace App\Services\Update\Joomla\Joomla5;

use App\Models\Update\Joomla\Joomla5\Joomla5ExtensionsInfoModel;
use App\Services\Update\Joomla\Joomla5\Joomla5DownloadLinkService;
use App\Services\Update\Joomla\Joomla5\Joomla5TargetPlatformsService;

// Joomla5ExtensionsInfoProviders
// Joomla5ExtensionsInfoServices
// Joomla5ExtensionsInfoModel
/**
 * Service class for Joomla 5 extensions information.
 */
class Joomla5ExtensionsInfoService
{
    protected $joomla5ExtensionsInfoModel;
    protected $joomla5DownloadLinkService;
    protected $joomla5TargetPlatformsService;

    /**
     * Constructor.
     * 
     * @param Joomla5ExtensionsInfoModel $model The model for Joomla 5 extensions information.
     */
    public function __construct(
        Joomla5ExtensionsInfoModel $joomla5ExtensionsInfoModel,
        Joomla5DownloadLinkService $joomla5DownloadLinkService,
        Joomla5TargetPlatformsService $joomla5TargetPlatformsService
    ) {
        $this->joomla5ExtensionsInfoModel = $joomla5ExtensionsInfoModel;
        $this->joomla5DownloadLinkService = $joomla5DownloadLinkService;
        $this->joomla5TargetPlatformsService = $joomla5TargetPlatformsService;
    }

    /**
     * Get all extensions.
     * 
     * @return array The array of all extensions.
     */
    public function getAllExtensions(): array
    {
        return $this->joomla5ExtensionsInfoModel->getAllExtensions();
    }

    /**
     * Get extension by ID.
     * 
     * @param int $id The ID of the extension.
     * @return array The array of the extension.
     */
    public function getExtensionById(int $id): array
    {
        return $this->joomla5ExtensionsInfoModel->getExtensionById($id);
    }

    /**
     * Get extension by element.
     * 
     * @param string $element The element of the extension.
     * @return array The array of the extension.
     */
    public function getExtensionByElement(string $element): array
    {
        return $this->joomla5ExtensionsInfoModel->getExtensionByElement($element);
    }

    /**
     * Get the last version for a given element.
     *
     * @param string $element The element for which to get the last version.
     * @return array The last version for the given element.
     */
    public function getLastVersionForElement(string $element)
    {
        $getLastVersionForElement = $this->joomla5ExtensionsInfoModel->getLastVersionForElement($element);
        if (count($getLastVersionForElement)) {
            foreach ($getLastVersionForElement as $key => $value) {
                $getDownloadLinksByExtensionId = $this->joomla5DownloadLinkService->getDownloadLinksByExtensionId($value["id"]);
                if ($getDownloadLinksByExtensionId) {
                    $getLastVersionForElement[$key]['downloads'] = $this->joomla5DownloadLinkService->getDownloadLinksByExtensionId($value["id"]);
                }
                $getTargetPlatformsByExtensionId = $this->joomla5TargetPlatformsService->getTargetPlatformsByExtensionId($value["id"]);
                if ($getTargetPlatformsByExtensionId) {
                    $getLastVersionForElement[$key]['targetplatform'] = $this->joomla5TargetPlatformsService->getTargetPlatformsByExtensionId($value["id"]);
                }
            }
        }
        return $getLastVersionForElement;
    }

    /**
     * Create extension.
     * 
     * @param array $data The data for creating the extension.
     * @return bool True if the extension is created successfully, false otherwise.
     */
    public function createExtension(array $data): bool
    {
        return $this->joomla5ExtensionsInfoModel->createExtension($data);
    }

    /**
     * Update extension.
     * 
     * @param int $id The ID of the extension to update.
     * @param array $data The data for updating the extension.
     * @return bool True if the extension is updated successfully, false otherwise.
     */
    public function updateExtension(int $id, array $data): bool
    {
        return $this->joomla5ExtensionsInfoModel->updateExtension($id, $data);
    }

    /**
     * Delete extension.
     * 
     * @param int $id The ID of the extension to delete.
     * @return bool True if the extension is deleted successfully, false otherwise.
     */
    public function deleteExtension(int $id): bool
    {
        return $this->joomla5ExtensionsInfoModel->deleteExtension($id);
    }
}
