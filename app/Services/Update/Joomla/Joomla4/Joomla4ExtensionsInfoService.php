<?php

namespace App\Services\Update\Joomla\Joomla4;

use App\Models\Update\Joomla\Joomla4\Joomla4ExtensionsInfoModel;
use App\Services\Update\Joomla\Joomla4\Joomla4DownloadLinkService;
use App\Services\Update\Joomla\Joomla4\Joomla4TargetPlatformsService;

// Joomla4ExtensionsInfoProviders
// Joomla4ExtensionsInfoServices
// Joomla4ExtensionsInfoModel
/**
 * Service class for Joomla 4 extensions information.
 */
class Joomla4ExtensionsInfoService
{
    protected $joomla4ExtensionsInfoModel;
    protected $joomla4DownloadLinkService;
    protected $joomla4TargetPlatformsService;

    /**
     * Constructor.
     * 
     * @param Joomla4ExtensionsInfoModel $model The model for Joomla 4 extensions information.
     */
    public function __construct(
        Joomla4ExtensionsInfoModel $joomla4ExtensionsInfoModel,
        Joomla4DownloadLinkService $joomla4DownloadLinkService,
        Joomla4TargetPlatformsService $joomla4TargetPlatformsService
    ) {
        $this->joomla4ExtensionsInfoModel = $joomla4ExtensionsInfoModel;
        $this->joomla4DownloadLinkService = $joomla4DownloadLinkService;
        $this->joomla4TargetPlatformsService = $joomla4TargetPlatformsService;
    }

    /**
     * Get all extensions.
     * 
     * @return array The array of all extensions.
     */
    public function getAllExtensions(): array
    {
        return $this->joomla4ExtensionsInfoModel->getAllExtensions();
    }

    /**
     * Get extension by ID.
     * 
     * @param int $id The ID of the extension.
     * @return array The array of the extension.
     */
    public function getExtensionById(int $id): array
    {
        return $this->joomla4ExtensionsInfoModel->getExtensionById($id);
    }

    /**
     * Get extension by element.
     * 
     * @param string $element The element of the extension.
     * @return array The array of the extension.
     */
    public function getExtensionByElement(string $element): array
    {
        return $this->joomla4ExtensionsInfoModel->getExtensionByElement($element);
    }

    /**
     * Get the last version for a given element.
     *
     * @param string $element The element for which to get the last version.
     * @return array The last version for the given element.
     */
    public function getLastVersionForElement(string $element)
    {
        $getLastVersionForElement = $this->joomla4ExtensionsInfoModel->getLastVersionForElement($element);
        if (count($getLastVersionForElement)) {
            foreach ($getLastVersionForElement as $key => $value) {
                $getDownloadLinksByExtensionId = $this->joomla4DownloadLinkService->getDownloadLinksByExtensionId($value["id"]);
                if ($getDownloadLinksByExtensionId) {
                    $getLastVersionForElement[$key]['downloads'] = $this->joomla4DownloadLinkService->getDownloadLinksByExtensionId($value["id"]);
                }
                $getTargetPlatformsByExtensionId = $this->joomla4TargetPlatformsService->getTargetPlatformsByExtensionId($value["id"]);
                if ($getTargetPlatformsByExtensionId) {
                    $getLastVersionForElement[$key]['targetplatform'] = $this->joomla4TargetPlatformsService->getTargetPlatformsByExtensionId($value["id"]);
                }
            }
        }
        return $getLastVersionForElement;
    }

    /**
     * Create extension.
     * 
     * @param array $data The data for creating the extension.
     * @return bool True if the extension is created successfully, false otherwise.
     */
    public function createExtension(array $data): bool
    {
        return $this->joomla4ExtensionsInfoModel->createExtension($data);
    }

    /**
     * Update extension.
     * 
     * @param int $id The ID of the extension to update.
     * @param array $data The data for updating the extension.
     * @return bool True if the extension is updated successfully, false otherwise.
     */
    public function updateExtension(int $id, array $data): bool
    {
        return $this->joomla4ExtensionsInfoModel->updateExtension($id, $data);
    }

    /**
     * Delete extension.
     * 
     * @param int $id The ID of the extension to delete.
     * @return bool True if the extension is deleted successfully, false otherwise.
     */
    public function deleteExtension(int $id): bool
    {
        return $this->joomla4ExtensionsInfoModel->deleteExtension($id);
    }
}
