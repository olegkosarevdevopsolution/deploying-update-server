<?php

namespace App\Services\Update\Joomla\Joomla4;

use App\Models\Update\Joomla\Joomla4\Joomla4TargetPlatformsModel;

/**
 * Joomla4TargetPlatformsService
 *
 * description
 *
 * @package \App\Services\Update\Joomla\Joomla4;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2024 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/Deploying%20Update%20Server
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class Joomla4TargetPlatformsService
{
    protected $joomla4TargetPlatformsModel;

    public function __construct(Joomla4TargetPlatformsModel $joomla4TargetPlatformsModel)
    {
        $this->joomla4TargetPlatformsModel = $joomla4TargetPlatformsModel;
    }

    public function getAllPlatforms()
    {
        return $this->joomla4TargetPlatformsModel->getAll();
    }

    public function getPlatformById($id)
    {
        return $this->joomla4TargetPlatformsModel->getById($id);
    }

    public function getTargetPlatformsByExtensionId($extensionId)
    {
        return $this->joomla4TargetPlatformsModel->getTargetPlatformsByExtensionId($extensionId);
    }

    public function createPlatform($data)
    {
        return $this->joomla4TargetPlatformsModel->insertPlatform($data);
    }

    public function updatePlatform($id, $data)
    {
        return $this->joomla4TargetPlatformsModel->updatePlatform($id, $data);
    }

    public function deletePlatform($id)
    {
        return $this->joomla4TargetPlatformsModel->deletePlatform($id);
    }
}
