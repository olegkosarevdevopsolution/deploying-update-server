<?php

namespace App\Services\Update\Joomla;

use App\Models\Update\Joomla\PhpVersionsModel;

/**
 * Service class for PHP versions.
 */
class PhpVersionsServices
{
    protected $model;

    /**
     * Constructor.
     * 
     * @param PhpVersionsModel $model The model for PHP versions.
     */
    public function __construct(PhpVersionsModel $model)
    {
        $this->model = $model;
    }

    /**
     * Get all PHP versions.
     * 
     * @return array The array of all PHP versions.
     */
    public function getAllVersions(): array
    {
        return $this->model->getAllVersions();
    }

    /**
     * Get PHP version by ID.
     * 
     * @param int $id The ID of the PHP version.
     * @return array The array of the PHP version.
     */
    public function getVersionById(int $id): array
    {
        return $this->model->getVersionById($id);
    }

    /**
     * Create PHP version.
     * 
     * @param array $data The data for creating the PHP version.
     * @return bool True if the PHP version is created successfully, false otherwise.
     */
    public function createVersion(array $data): bool
    {
        return $this->model->createVersion($data);
    }

    /**
     * Update PHP version.
     * 
     * @param int $id The ID of the PHP version to update.
     * @param array $data The data for updating the PHP version.
     * @return bool True if the PHP version is updated successfully, false otherwise.
     */
    public function updateVersion(int $id, array $data): bool
    {
        return $this->model->updateVersion($id, $data);
    }

    /**
     * Delete PHP version.
     * 
     * @param int $id The ID of the PHP version to delete.
     * @return bool True if the PHP version is deleted successfully, false otherwise.
     */
    public function deleteVersion(int $id): bool
    {
        return $this->model->deleteVersion($id);
    }
}
