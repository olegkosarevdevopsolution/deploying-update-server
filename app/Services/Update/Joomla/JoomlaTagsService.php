<?php

namespace App\Services;

use App\Models\Update\Joomla\JoomlaTagsModel;

/**
 * Service class for Joomla tags.
 */
class JoomlaTagsService
{
    protected $model;

    /**
     * Constructor.
     * 
     * @param JoomlaTagsModel $model The model for Joomla tags.
     */
    public function __construct(JoomlaTagsModel $model)
    {
        $this->model = $model;
    }

    /**
     * Get all Joomla tags.
     * 
     * @return array The array of all Joomla tags.
     */
    public function getAllTags(): array
    {
        return $this->model->getAllTags();
    }

    /**
     * Get Joomla tag by ID.
     * 
     * @param mixed $id The ID of the Joomla tag.
     * @return array The array of the Joomla tag.
     */
    public function getTagById($id): array
    {
        return $this->model->getTagById($id);
    }

    /**
     * Create Joomla tag.
     * 
     * @param array $data The data for creating the Joomla tag.
     * @return bool True if the Joomla tag is created successfully, false otherwise.
     */
    public function createTag($data): bool
    {
        return $this->model->createTag($data);
    }

    /**
     * Update Joomla tag.
     * 
     * @param mixed $id The ID of the Joomla tag to update.
     * @param array $data The data for updating the Joomla tag.
     * @return bool True if the Joomla tag is updated successfully, false otherwise.
     */
    public function updateTag($id, $data): bool
    {
        return $this->model->updateTag($id, $data);
    }

    /**
     * Delete Joomla tag.
     * 
     * @param mixed $id The ID of the Joomla tag to delete.
     * @return bool True if the Joomla tag is deleted successfully, false otherwise.
     */
    public function deleteTag($id): bool
    {
        return $this->model->deleteTag($id);
    }
}
