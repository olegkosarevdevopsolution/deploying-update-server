<?php

namespace App\Services\Update\Joomla\Joomla3;

use App\Models\Update\Joomla\Joomla3\Joomla3ExtensionsInfoModel;
use App\Services\Update\Joomla\Joomla3\Joomla3DownloadLinkService;
use App\Services\Update\Joomla\Joomla3\Joomla3TargetPlatformsService;

// Joomla3ExtensionsInfoProviders
// Joomla3ExtensionsInfoServices
// Joomla3ExtensionsInfoModel
/**
 * Service class for Joomla 3 extensions information.
 */
class Joomla3ExtensionsInfoService
{
    protected $joomla3ExtensionsInfoModel;
    protected $joomla3DownloadLinkService;
    protected $joomla3TargetPlatformsService;

    /**
     * Constructor.
     * 
     * @param Joomla3ExtensionsInfoModel $model The model for Joomla 3 extensions information.
     */
    public function __construct(
        Joomla3ExtensionsInfoModel $joomla3ExtensionsInfoModel,
        Joomla3DownloadLinkService $joomla3DownloadLinkService,
        Joomla3TargetPlatformsService $joomla3TargetPlatformsService
    ) {
        $this->joomla3ExtensionsInfoModel = $joomla3ExtensionsInfoModel;
        $this->joomla3DownloadLinkService = $joomla3DownloadLinkService;
        $this->joomla3TargetPlatformsService = $joomla3TargetPlatformsService;
    }

    /**
     * Get all extensions.
     * 
     * @return array The array of all extensions.
     */
    public function getAllExtensions(): array
    {
        return $this->joomla3ExtensionsInfoModel->getAllExtensions();
    }

    /**
     * Get extension by ID.
     * 
     * @param int $id The ID of the extension.
     * @return array The array of the extension.
     */
    public function getExtensionById(int $id): array
    {
        return $this->joomla3ExtensionsInfoModel->getExtensionById($id);
    }

    /**
     * Get extension by element.
     * 
     * @param string $element The element of the extension.
     * @return array The array of the extension.
     */
    public function getExtensionByElement(string $element): array
    {
        return $this->joomla3ExtensionsInfoModel->getExtensionByElement($element);
    }

    /**
     * Get the last version for a given element.
     *
     * @param string $element The element for which to get the last version.
     * @return array The last version for the given element.
     */
    public function getLastVersionForElement(string $element)
    {
        $getLastVersionForElement = $this->joomla3ExtensionsInfoModel->getLastVersionForElement($element);
        if (count($getLastVersionForElement)) {
            foreach ($getLastVersionForElement as $key => $value) {
                $getDownloadLinksByExtensionId = $this->joomla3DownloadLinkService->getDownloadLinksByExtensionId($value["id"]);
                if ($getDownloadLinksByExtensionId) {
                    $getLastVersionForElement[$key]['downloads'] = $this->joomla3DownloadLinkService->getDownloadLinksByExtensionId($value["id"]);
                }
                $getTargetPlatformsByExtensionId = $this->joomla3TargetPlatformsService->getTargetPlatformsByExtensionId($value["id"]);
                if ($getTargetPlatformsByExtensionId) {
                    $getLastVersionForElement[$key]['targetplatform'] = $this->joomla3TargetPlatformsService->getTargetPlatformsByExtensionId($value["id"]);
                }
            }
        }
        return $getLastVersionForElement;
    }

    /**
     * Create extension.
     * 
     * @param array $data The data for creating the extension.
     * @return bool True if the extension is created successfully, false otherwise.
     */
    public function createExtension(array $data): bool
    {
        return $this->joomla3ExtensionsInfoModel->createExtension($data);
    }

    /**
     * Update extension.
     * 
     * @param int $id The ID of the extension to update.
     * @param array $data The data for updating the extension.
     * @return bool True if the extension is updated successfully, false otherwise.
     */
    public function updateExtension(int $id, array $data): bool
    {
        return $this->joomla3ExtensionsInfoModel->updateExtension($id, $data);
    }

    /**
     * Delete extension.
     * 
     * @param int $id The ID of the extension to delete.
     * @return bool True if the extension is deleted successfully, false otherwise.
     */
    public function deleteExtension(int $id): bool
    {
        return $this->joomla3ExtensionsInfoModel->deleteExtension($id);
    }
}
