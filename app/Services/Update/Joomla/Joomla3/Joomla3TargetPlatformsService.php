<?php

namespace App\Services\Update\Joomla\Joomla3;

use App\Models\Update\Joomla\Joomla3\Joomla3TargetPlatformsModel;

/**
 * Joomla3TargetPlatformsService
 *
 * description
 *
 * @package \App\Services\Update\Joomla\Joomla3;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2024 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/Deploying%20Update%20Server
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class Joomla3TargetPlatformsService
{
    protected $joomla3TargetPlatformsModel;

    public function __construct(Joomla3TargetPlatformsModel $joomla3TargetPlatformsModel)
    {
        $this->joomla3TargetPlatformsModel = $joomla3TargetPlatformsModel;
    }

    public function getAllPlatforms()
    {
        return $this->joomla3TargetPlatformsModel->getAll();
    }

    public function getPlatformById($id)
    {
        return $this->joomla3TargetPlatformsModel->getById($id);
    }

    public function getTargetPlatformsByExtensionId($extensionId)
    {
        return $this->joomla3TargetPlatformsModel->getTargetPlatformsByExtensionId($extensionId);
    }

    public function createPlatform($data)
    {
        return $this->joomla3TargetPlatformsModel->insertPlatform($data);
    }

    public function updatePlatform($id, $data)
    {
        return $this->joomla3TargetPlatformsModel->updatePlatform($id, $data);
    }

    public function deletePlatform($id)
    {
        return $this->joomla3TargetPlatformsModel->deletePlatform($id);
    }
}
