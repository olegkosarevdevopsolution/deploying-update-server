<?php

namespace App\Models\Update\Joomla;

use CodeIgniter\Model;

/**
 * Model class for Joomla tags.
 */
class JoomlaTagsModel extends Model
{
    protected $table = 'joomla_tags';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'name'
    ];

    /**
     * Get all Joomla tags.
     * 
     * @return array The array of all Joomla tags.
     */
    public function getAllTags(): array
    {
        return $this->findAll();
    }

    /**
     * Get Joomla tag by ID.
     * 
     * @param int $id The ID of the Joomla tag.
     * @return array The array of the Joomla tag.
     */
    public function getTagById(int $id): array
    {
        return $this->find($id);
    }

    /**
     * Create Joomla tag.
     * 
     * @param array $data The data for creating the Joomla tag.
     * @return bool True if the Joomla tag is created successfully, false otherwise.
     */
    public function createTag(array $data): bool
    {
        return $this->insert($data);
    }

    /**
     * Update Joomla tag.
     * 
     * @param int $id The ID of the Joomla tag to update.
     * @param array $data The data for updating the Joomla tag.
     * @return bool True if the Joomla tag is updated successfully, false otherwise.
     */
    public function updateTag(int $id, array $data): bool
    {
        return $this->update($id, $data);
    }

    /**
     * Delete Joomla tag.
     * 
     * @param int $id The ID of the Joomla tag to delete.
     * @return bool True if the Joomla tag is deleted successfully, false otherwise.
     */
    public function deleteTag(int $id): bool
    {
        return $this->delete($id);
    }
}
