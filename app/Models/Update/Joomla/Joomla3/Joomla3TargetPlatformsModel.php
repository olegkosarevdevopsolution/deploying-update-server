<?php

namespace App\Models\Update\Joomla\Joomla3;

use CodeIgniter\Model;

/**
 * Joomla3TargetPlatformsModel
 *
 * description
 *
 * @package \App\Models\Update\Joomla\Joomla3;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2024 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/Deploying%20Update%20Server
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class Joomla3TargetPlatformsModel extends Model
{
    protected $table = 'joomla3_target_platforms';
    protected $primaryKey = 'id';
    protected $allowedFields = ['extension_id', 'name', 'version', 'min_dev_level', 'max_dev_level'];

    public function getAll()
    {
        return $this->findAll();
    }

    public function getById($id)
    {
        return $this->find($id);
    }

    public function getTargetPlatformsByExtensionId($extensionId)
    {
        return $this->where('extension_id', $extensionId)->first();
    }

    public function insertPlatform($data)
    {
        return $this->insert($data);
    }

    public function updatePlatform($id, $data)
    {
        return $this->update($id, $data);
    }

    public function deletePlatform($id)
    {
        return $this->delete($id);
    }
}
