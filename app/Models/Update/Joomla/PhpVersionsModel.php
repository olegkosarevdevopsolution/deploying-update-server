<?php

namespace App\Models\Update\Joomla;

use CodeIgniter\Model;

/**
 * Model class for PHP versions.
 */
class PhpVersionsModel extends Model
{
    /**
     * The name of the database table for this model.
     *
     * @var string
     */
    protected $table = 'php_versions';

    /**
     * The primary key for the database table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The fields that are allowed to be mass assigned when inserting or updating records.
     *
     * @var array
     */
    protected $allowedFields = [
        'version'
    ];

    /**
     * Get all PHP versions.
     * 
     * @return array The array of all PHP versions.
     */
    public function getAllVersions(): array
    {
        return $this->findAll();
    }

    /**
     * Get PHP version by ID.
     * 
     * @param int $id The ID of the PHP version.
     * @return array The array of the PHP version.
     */
    public function getVersionById(int $id): array
    {
        return $this->find($id);
    }

    /**
     * Create PHP version.
     * 
     * @param array $data The data for creating the PHP version.
     * @return bool True if the PHP version is created successfully, false otherwise.
     */
    public function createVersion(array $data): bool
    {
        return $this->insert($data);
    }

    /**
     * Update PHP version.
     * 
     * @param int $id The ID of the PHP version to update.
     * @param array $data The data for updating the PHP version.
     * @return bool True if the PHP version is updated successfully, false otherwise.
     */
    public function updateVersion(int $id, array $data): bool
    {
        return $this->update($id, $data);
    }

    /**
     * Delete PHP version.
     * 
     * @param int $id The ID of the PHP version to delete.
     * @return bool True if the PHP version is deleted successfully, false otherwise.
     */
    public function deleteVersion(int $id): bool
    {
        return $this->delete($id);
    }
}
