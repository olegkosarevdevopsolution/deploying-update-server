<?php

namespace App\Models\Update\Joomla\Joomla4;

use CodeIgniter\Model;

/**
 * Joomla4DownloadLinkModel
 *
 * description
 *
 * @package \App\Models\Update\Joomla\Joomla4;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2024 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/Deploying%20Update%20Server
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class Joomla4DownloadLinkModel extends Model
{
    protected $table = 'joomla4_download_links';
    protected $primaryKey = 'id';
    protected $allowedFields = ['extension_id', 'downloadurl', 'downloadsource', 'type', 'format'];

    public function getAllDownloadLinks()
    {
        return $this->findAll();
    }

    public function getDownloadLinkById($id)
    {
        return $this->find($id);
    }

    public function getDownloadLinksByExtensionId($extensionId)
    {
        return $this->where('extension_id', $extensionId)->findAll();
    }

    public function getDownloadUrlLinkByFileName(string $fileName)
    {
        return $this->like("downloadurl", $fileName, 'both')->first();
    }

    public function createDownloadLink($data)
    {
        return $this->insert($data);
    }

    public function updateDownloadLink($id, $data)
    {
        return $this->update($id, $data);
    }

    public function deleteDownloadLink($id)
    {
        return $this->delete($id);
    }
}
