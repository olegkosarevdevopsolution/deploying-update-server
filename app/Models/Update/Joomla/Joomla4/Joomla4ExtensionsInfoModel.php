<?php

namespace App\Models\Update\Joomla\Joomla4;

use CodeIgniter\Model;

class Joomla4ExtensionsInfoModel extends Model
{
    /**
     * The name of the database table for this model.
     *
     * @var string
     */
    protected $table = 'joomla4_extensions_info';

    /**
     * The primary key for the database table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The fields that are allowed to be mass assigned when inserting or updating records.
     *
     * @var array
     */
    protected $allowedFields = [
        'name',
        'description',
        'element',
        'type',
        'folder',
        'client',
        'version',
        'infourl',
        'joomla_tag_name',
        'changelogurl',
        'maintainer',
        'maintainerurl',
        'php_minimum_version',
        'sha256',
        'sha384',
        'sha512',
        'created_at',
        'updated_at',
    ];

    /**
     * Whether to use timestamp fields for created_at and updated_at.
     *
     * @var bool
     */
    protected $useTimestamps = true;

    /**
     * The field to use as the created timestamp.
     *
     * @var string
     */
    protected $createdField  = 'created_at';

    /**
     * The field to use as the updated timestamp.
     *
     * @var string
     */
    protected $updatedField  = 'updated_at';

    /**
     * The validation rules for the model's data.
     *
     * @var array
     */
    protected $validationRules = [];

    /**
     * The validation error messages for the model's data.
     *
     * @var array
     */
    protected $validationMessages = [];

    /**
     * Whether to skip validation for this model.
     *
     * @var bool
     */
    protected $skipValidation = false;

    /**
     * Get all extensions from the database.
     *
     * @return array
     */
    public function getAllExtensions(): array
    {
        return $this->findAll();
    }

    /**
     * Get an extension from the database by its ID.
     *
     * @param int $id The ID of the extension.
     * @return array
     */
    public function getExtensionById(int $id): array
    {
        return $this->find($id);
    }

    /**
     * Get an extension from the database by its element.
     *
     * @param string $element The element of the extension.
     * @return array
     */
    public function getExtensionByElement(string $element): array
    {
        return $this->where('element', $element)->first();
    }

    /**
     * Get the last version for the given element
     *
     * @param string $element The element to get the last version for
     * @return array The last version for the given element
     */
    public function getLastVersionForElement($element): array
    {
        $lastVersionNumber = $this
            ->select('version')
            ->where('element', $element)
            ->orderBy('version', 'desc')
            ->first();
        if ($lastVersionNumber) {
            return $this
                ->where('version', $lastVersionNumber['version'])
                ->where('element', $element)
                ->findAll();
        }
        return [];
    }

    /**
     * Create a new extension in the database.
     *
     * @param array $data The data for the new extension.
     * @return boolean
     */
    public function createExtension(array $data): bool
    {
        return $this->insert($data);
    }

    /**
     * Update an extension in the database by its ID.
     *
     * @param int $id The ID of the extension.
     * @param array $data The updated data for the extension.
     * @return boolean
     */
    public function updateExtension(int $id, array $data): bool
    {
        return $this->update($id, $data);
    }

    /**
     * Delete an extension from the database by its ID.
     *
     * @param int $id The ID of the extension to be deleted.
     * @return boolean
     */
    public function deleteExtension(int $id): bool
    {
        return $this->delete($id);
    }
}
