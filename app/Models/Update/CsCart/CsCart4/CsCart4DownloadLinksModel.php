<?php

namespace App\Models\Update\CsCart\CsCart4;

use CodeIgniter\Model;

/**
 * CsCart4DownloadLinksModel
 *
 * description
 *
 * @package \App\Models\Update\CsCart\CsCart4;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2024 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/Deploying%20Update%20Server
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class CsCart4DownloadLinksModel extends Model
{
    protected $table = 'cscart4_download_links';
    protected $primaryKey = 'id';
    protected $allowedFields = ['extension_id', 'downloadurl', 'format'];
    protected $returnType = 'array';

    public function getAllDownloadLinks()
    {
        return $this->findAll();
    }

    public function getDownloadLinkById(int $id)
    {
        return $this->find($id);
    }

    public function getDownloadLinkByAddonId(string $addon_id)
    {
        return $this->where('addon_id', $addon_id)->first();
    }

    public function getDownloadUrlLinkByFileName(string $fileName)
    {
        return $this->like("downloadurl", $fileName, 'both')->first();
    }

    public function createDownloadLink(array $data)
    {
        return $this->insert($data);
    }

    public function updateDownloadLink(int $id, array $data)
    {
        return $this->update($id, $data);
    }

    public function deleteDownloadLink(int $id)
    {
        return $this->delete($id);
    }
}
