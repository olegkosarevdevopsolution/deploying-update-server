<?php

namespace App\Models\Update\CsCart\CsCart4;

use CodeIgniter\Model;

/**
 * CsCart4AddonsInfoModel
 *
 * description
 *
 * @package \App\Models\Update\CsCart\CsCart4;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2024 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/Deploying%20Update%20Server
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class CsCart4AddonsInfoModel extends Model
{
    protected $table = 'cscart4_addons_info';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'element',
        'name',
        'description',
        'from_version',
        'to_version',
        'timestamp',
        'size',
        'created_at',
        'updated_at'
    ];
    protected $returnType = 'array';

    public function getAllAddonsInfo()
    {
        return $this->findAll();
    }

    public function getAddonInfoById(int $id)
    {
        return $this->find($id);
    }

    public function getAddonInfoByElement(string $element)
    {
        return $this->where('element', $element)->findAll();
    }

    /**
     * Get the last version for the given element
     *
     * @param string $element The element to get the last version for
     * @return array The last version for the given element
     */
    public function getLastVersionForElement($element): array
    {
        $lastVersionNumber = $this
            ->select('to_version')
            ->where('element', $element)
            ->orderBy('to_version', 'desc')
            ->first();
        if ($lastVersionNumber) {
            return $this
                ->where('to_version', $lastVersionNumber['to_version'])
                ->where('element', $element)
                ->first();
        }
        return [];
    }

    public function createAddonInfo(array $data)
    {
        return $this->insert($data);
    }

    public function updateAddonInfo(int $id, array $data)
    {
        return $this->update($id, $data);
    }

    public function deleteAddonInfo(int $id)
    {
        return $this->delete($id);
    }
}
