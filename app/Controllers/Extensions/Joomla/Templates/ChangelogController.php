<?php

namespace App\Controllers\Extensions\Joomla\Templates;

use App\Abstracts\UpdateRouteHandlerAbstract;

/**
 * ChangelogController
 *
 * description
 *
 * @package  \App\Controllers\Extensions\Joomla\Templates;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/Deploying%20Update%20Server
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class ChangelogController extends UpdateRouteHandlerAbstract
{

    /**
     * Constructor for the class.
     */
    public function __construct()
    {
    }

    /**
     * Handle the given name and return the clean name with prefix.
     *
     * @param string $name The input name to be handled.
     * @return string The clean name with prefix.
     */
    public function handle(string $name): string
    {
        return $this->setCleanName('.html', $name)->setPrefix('tpl_')->getCleanNameWithPrefix();
    }

    public function changelog(string $name): string
    {
        echo "Hello changelog ";
        var_dump($this->handle($name));
        var_dump($name);
        exit();
    }
}
