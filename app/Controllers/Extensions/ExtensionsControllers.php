<?php

namespace App\Controllers\Update\Extensions;

use App\Controllers\BaseController;

/**
 * ExtensionsControllers
 *
 * description
 *
 * @package  \App\Controllers\Update\Extensions;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2024 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/Deploying%20Update%20Server
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

class ExtensionsControllers extends BaseController
{

    /**
     * Constructor for the class.
     */
    public function __construct()
    {
    }

    public function index()
    {
        var_dump("All extensions");
    }
}
