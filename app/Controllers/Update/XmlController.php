<?php

namespace App\Controllers\Update;

use InvalidArgumentException;
use SimpleXMLElement;
use App\Controllers\BaseController;
use \CodeIgniter\HTTP\ResponseInterface;

class XmlController extends BaseController
{
    public function __construct()
    {
    }

    /**
     * Renders the given SimpleXMLElement as XML and sets it as the body of the given ResponseInterface.
     *
     * @param SimpleXMLElement $xml The XML to render
     * @param ResponseInterface $response The response to set the XML body on
     * @throws InvalidArgumentException If the $xml or $response is null
     * @return ResponseInterface The response with the XML body set
     */
    public function xmlRender(SimpleXMLElement $xml = null, ResponseInterface $response = null): ResponseInterface
    {
        if ($xml === null) {
            throw new InvalidArgumentException('XML object is null');
        }

        if ($response === null) {
            throw new InvalidArgumentException('Response object is null');
        }

        return $this->setResponse($xml->asXML(), $response);
    }

    /**
     * Renders an XML string to a ResponseInterface object.
     *
     * @param string $xml The XML string to render
     * @param ResponseInterface $response The response object to render the XML to
     * @throws InvalidArgumentException When the XML string or response object is null
     * @return ResponseInterface The rendered response object
     */
    public function xmlRenderString(string $xml = null, ResponseInterface $response = null): ResponseInterface
    {
        if ($xml === null) {
            throw new InvalidArgumentException('XML string is null');
        }

        if ($response === null) {
            throw new InvalidArgumentException('Response object is null');
        }

        return $this->setResponse($xml, $response);
    }

    /**
     * Set the XML response for the given ResponseInterface.
     *
     * @param string $xml The XML content to set in the response
     * @param ResponseInterface $response The response to set the XML content to
     * @return ResponseInterface The updated response object
     */
    private function setResponse(string $xml, ResponseInterface $response): ResponseInterface
    {
        $response->setHeader('Content-Type', 'text/xml');
        return $response->setBody($xml);
    }
}
