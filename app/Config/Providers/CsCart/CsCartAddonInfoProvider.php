<?php

namespace App\Config\Providers\CsCart;

use App\Services\Update\CsCart\CsCart4\CsCart4DownloadLinksService;
use App\Models\Update\CsCart\CsCart4\CsCart4AddonsInfoModel;
use App\Models\Update\CsCart\CsCart4\CsCart4DownloadLinksModel;
use App\Services\Update\CsCart\CsCart4\CsCart4AddonsInfoService;

/**
 * Class CsCartAddonInfoProvider
 * 
 * Provides methods to retrieve different Joomla extensions info services.
 */
class CsCartAddonInfoProvider
{

    /**
     * Get the CsCart4 extensions info service.
     * 
     * @return CsCart4AddonsInfoService
     */
    public static function csCart4AddonInfoService(): CsCart4AddonsInfoService
    {
        return
            new CsCart4AddonsInfoService(
                new CsCart4AddonsInfoModel(),
                new CsCart4DownloadLinksService(new CsCart4DownloadLinksModel()),
            );
    }
}
