<?php

namespace App\Config\Providers\CsCart;

use App\Models\Update\CsCart\CsCart4\CsCart4DownloadLinksModel;
use App\Services\Update\CsCart\CsCart4\CsCart4DownloadLinksService;

class CsCartDownloadLinkProvider
{

    public static function csCart4DownloadLinkService(): CsCart4DownloadLinksService
    {
        return new CsCart4DownloadLinksService(new CsCart4DownloadLinksModel());
    }
}
