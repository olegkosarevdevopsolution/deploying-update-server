<?php

namespace App\Config\Providers\Joomla;

use App\Models\Update\Joomla\Joomla3\Joomla3DownloadLinkModel;
use App\Services\Update\Joomla\Joomla3\Joomla3DownloadLinkService;

use App\Models\Update\Joomla\Joomla4\Joomla4DownloadLinkModel;
use App\Services\Update\Joomla\Joomla4\Joomla4DownloadLinkService;

use App\Models\Update\Joomla\Joomla5\Joomla5DownloadLinkModel;
use App\Services\Update\Joomla\Joomla5\Joomla5DownloadLinkService;

class JoomlaDownloadLinkProvider
{
    public static function joomla3DownloadLinkService(): Joomla3DownloadLinkService
    {
        return new Joomla3DownloadLinkService(new Joomla3DownloadLinkModel());
    }

    public static function joomla4DownloadLinkService(): Joomla4DownloadLinkService
    {
        return new Joomla4DownloadLinkService(new Joomla4DownloadLinkModel());
    }

    public static function joomla5DownloadLinkService(): Joomla5DownloadLinkService
    {
        return new Joomla5DownloadLinkService(new Joomla5DownloadLinkModel());
    }
}
