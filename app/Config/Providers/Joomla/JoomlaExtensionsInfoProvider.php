<?php

namespace App\Config\Providers\Joomla;

use App\Database\Migrations\Joomla4\Joomla4DownloadsLinks;
use App\Models\Update\Joomla\Joomla1\Joomla1ExtensionsInfoModel;
use App\Services\Update\Joomla\Joomla1\Joomla1ExtensionsInfoService;

use App\Models\Update\Joomla\Joomla25\Joomla25ExtensionsInfoModel;
use App\Services\Update\Joomla\Joomla25\Joomla25ExtensionsInfoService;

use App\Models\Update\Joomla\Joomla3\Joomla3ExtensionsInfoModel;
use App\Services\Update\Joomla\Joomla3\Joomla3ExtensionsInfoService;
use App\Models\Update\Joomla\Joomla3\Joomla3DownloadLinkModel;
use App\Services\Update\Joomla\Joomla3\Joomla3DownloadLinkService;
use App\Models\Update\Joomla\Joomla3\Joomla3TargetPlatformsModel;
use App\Services\Update\Joomla\Joomla3\Joomla3TargetPlatformsService;

use App\Models\Update\Joomla\Joomla4\Joomla4ExtensionsInfoModel;
use App\Services\Update\Joomla\Joomla4\Joomla4ExtensionsInfoService;
use App\Models\Update\Joomla\Joomla4\Joomla4DownloadLinkModel;
use App\Services\Update\Joomla\Joomla4\Joomla4DownloadLinkService;
use App\Models\Update\Joomla\Joomla4\Joomla4TargetPlatformsModel;
use App\Services\Update\Joomla\Joomla4\Joomla4TargetPlatformsService;

use App\Models\Update\Joomla\Joomla5\Joomla5ExtensionsInfoModel;
use App\Services\Update\Joomla\Joomla5\Joomla5ExtensionsInfoService;
use App\Models\Update\Joomla\Joomla5\Joomla5DownloadLinkModel;
use App\Services\Update\Joomla\Joomla5\Joomla5DownloadLinkService;
use App\Models\Update\Joomla\Joomla5\Joomla5TargetPlatformsModel;
use App\Services\Update\Joomla\Joomla5\Joomla5TargetPlatformsService;

/**
 * Class JoomlaExtensionsInfoProvider
 * 
 * Provides methods to retrieve different Joomla extensions info services.
 */
class JoomlaExtensionsInfoProvider
{
    /**
     * Get the Joomla1 extensions info service.
     * 
     * @return Joomla1ExtensionsInfoService
     */
    // public static function joomla1ExtensionsInfoService(): Joomla1ExtensionsInfoService
    // {
    //     return new Joomla1ExtensionsInfoService(new Joomla1ExtensionsInfoModel());
    // }

    /**
     * Get the Joomla25 extensions info service.
     * 
     * @return Joomla25ExtensionsInfoService
     */
    // public static function joomla25ExtensionsInfoService(): Joomla25ExtensionsInfoService
    // {
    //     return new Joomla25ExtensionsInfoService(new Joomla25ExtensionsInfoModel());
    // }

    /**
     * Get the Joomla3 extensions info service.
     * 
     * @return Joomla3ExtensionsInfoService
     */
    public static function joomla3ExtensionsInfoService(): Joomla3ExtensionsInfoService
    {
        return
            new Joomla3ExtensionsInfoService(
                new Joomla3ExtensionsInfoModel(),
                new Joomla3DownloadLinkService(new Joomla3DownloadLinkModel()),
                new Joomla3TargetPlatformsService(new Joomla3TargetPlatformsModel())
            );
    }

    /**
     * Get the Joomla4 extensions info service.
     * 
     * @return Joomla4ExtensionsInfoService
     */
    public static function joomla4ExtensionsInfoService(): Joomla4ExtensionsInfoService
    {
        return
            new Joomla4ExtensionsInfoService(
                new Joomla4ExtensionsInfoModel(),
                new Joomla4DownloadLinkService(new Joomla4DownloadLinkModel()),
                new Joomla4TargetPlatformsService(new Joomla4TargetPlatformsModel())
            );
    }

    /**
     * Get the Joomla5 extensions info service.
     * 
     * @return Joomla5ExtensionsInfoService
     */
    public static function joomla5ExtensionsInfoService(): Joomla5ExtensionsInfoService
    {
        return
            new Joomla5ExtensionsInfoService(
                new Joomla5ExtensionsInfoModel(),
                new Joomla5DownloadLinkService(new Joomla5DownloadLinkModel()),
                new Joomla5TargetPlatformsService(new Joomla5TargetPlatformsModel())
            );
    }
}
