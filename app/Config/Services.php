<?php

namespace Config;

use CodeIgniter\Config\BaseService;
use App\Services\InformationHandlerService;
use App\Config\Providers\CsCart\CsCartAddonInfoProvider;
use App\Config\Providers\CsCart\CsCartDownloadLinkProvider;
use App\Config\Providers\Joomla\JoomlaDownloadLinkProvider;
use App\Config\Providers\Joomla\JoomlaExtensionsInfoProvider;
use App\Services\Update\CsCart\CsCart4\CsCart4AddonsInfoService;
use App\Services\Update\Joomla\Joomla3\Joomla3DownloadLinkService;
use App\Services\Update\Joomla\Joomla4\Joomla4DownloadLinkService;
use App\Services\Update\Joomla\Joomla5\Joomla5DownloadLinkService;
use App\Services\Update\CsCart\CsCart4\CsCart4DownloadLinksService;
use App\Services\Update\Joomla\Joomla3\Joomla3ExtensionsInfoService;
use App\Services\Update\Joomla\Joomla4\Joomla4ExtensionsInfoService;
use App\Services\Update\Joomla\Joomla5\Joomla5ExtensionsInfoService;

/**
 * Services Configuration file.
 *
 * Services are simply other classes/libraries that the system uses
 * to do its job. This is used by CodeIgniter to allow the core of the
 * framework to be swapped out easily without affecting the usage within
 * the rest of your application.
 *
 * This file holds any application-specific services, or service overrides
 * that you might need. An example has been included with the general
 * method format you should use for your service methods. For more examples,
 * see the core Services file at system/Config/Services.php.
 */
class Services extends BaseService
{
    /*
     * public static function example($getShared = true)
     * {
     *     if ($getShared) {
     *         return static::getSharedInstance('example');
     *     }
     *
     *     return new \CodeIgniter\Example();
     * }
     */

    protected static function createJoomlaExtensionsInfoProvider(): JoomlaExtensionsInfoProvider
    {
        return new JoomlaExtensionsInfoProvider();
    }

    protected static function createJoomlaDownloadLinkProvider(): JoomlaDownloadLinkProvider
    {
        return new JoomlaDownloadLinkProvider();
    }

    protected static function createCsCartDownloadLinkProvider(): CsCartDownloadLinkProvider
    {
        return new CsCartDownloadLinkProvider();
    }

    protected static function createCsCartAddonInfoProvider(): CsCartAddonInfoProvider
    {
        return new CsCartAddonInfoProvider();
    }


    public static function joomla3ExtensionsInfoService($getShared = true): Joomla3ExtensionsInfoService
    {
        if ($getShared) {
            return static::getSharedInstance('joomla3ExtensionsInfoService');
        }
        $JoomlaExtensionsInfoProvider = static::createJoomlaExtensionsInfoProvider();
        return $JoomlaExtensionsInfoProvider->joomla3ExtensionsInfoService();
    }

    public static function joomla4ExtensionsInfoService($getShared = true): Joomla4ExtensionsInfoService
    {
        if ($getShared) {
            return static::getSharedInstance('joomla4ExtensionsInfoService');
        }
        $JoomlaExtensionsInfoProvider = static::createJoomlaExtensionsInfoProvider();
        return $JoomlaExtensionsInfoProvider->joomla4ExtensionsInfoService();
    }

    public static function joomla5ExtensionsInfoService($getShared = true): Joomla5ExtensionsInfoService
    {
        if ($getShared) {
            return static::getSharedInstance('joomla5ExtensionsInfoService');
        }
        $JoomlaExtensionsInfoProvider = static::createJoomlaExtensionsInfoProvider();
        return $JoomlaExtensionsInfoProvider->joomla5ExtensionsInfoService();
    }

    public static function joomla3DownloadLinkService($getShared = true): Joomla3DownloadLinkService
    {
        if ($getShared) {
            return static::getSharedInstance('joomla3DownloadLinkService');
        }
        $createJoomlaDownloadLinkProvider = static::createJoomlaDownloadLinkProvider();
        return $createJoomlaDownloadLinkProvider->joomla3DownloadLinkService();
    }

    public static function joomla4DownloadLinkService($getShared = true): Joomla4DownloadLinkService
    {
        if ($getShared) {
            return static::getSharedInstance('joomla4DownloadLinkService');
        }
        $createJoomlaDownloadLinkProvider = static::createJoomlaDownloadLinkProvider();
        return $createJoomlaDownloadLinkProvider->joomla4DownloadLinkService();
    }

    public static function joomla5DownloadLinkService($getShared = true): Joomla5DownloadLinkService
    {
        if ($getShared) {
            return static::getSharedInstance('joomla5DownloadLinkService');
        }
        $createJoomlaDownloadLinkProvider = static::createJoomlaDownloadLinkProvider();
        return $createJoomlaDownloadLinkProvider->joomla5DownloadLinkService();
    }

    public static function csCart4DownloadLinkService($getShared = true): CsCart4DownloadLinksService
    {
        if ($getShared) {
            return static::getSharedInstance('csCart4DownloadLinkService');
        }
        $createCsCartDownloadLinkProvider = static::createCsCartDownloadLinkProvider();
        return $createCsCartDownloadLinkProvider->csCart4DownloadLinkService();
    }

    public static function csCart4AddonInfoService($getShared = true): CsCart4AddonsInfoService
    {
        if ($getShared) {
            return static::getSharedInstance('csCart4AddonInfoService');
        }
        $createCsCartAddonInfoProvider = static::createCsCartAddonInfoProvider();
        return $createCsCartAddonInfoProvider->csCart4AddonInfoService();
    }

    public static function informationHandlerService($getShared = true): InformationHandlerService
    {
        if ($getShared) {
            return static::getSharedInstance('informationHandlerService');
        }
        return new InformationHandlerService(service('response'));
    }
}
