<?php

namespace Config\Routers\Extensions;


class ExtensionsRoutes
{
    protected \CodeIgniter\Router\RouteCollection $routes;
    public function __construct(\CodeIgniter\Router\RouteCollection $routes)
    {
        $this->routes = $routes;
    }

    protected function getRouteCollection(): \CodeIgniter\Router\RouteCollection
    {
        return $this->routes;
    }

    public function getRoutes()
    {
        $this->routes->group('extensions', ['namespace' => 'App\Controllers\Extensions'], function ($extensionsRoutes) {

            $joomlaRoute = new JoomlaRoutes($this->routes);
            $joomlaRoute->getRoute();
        });
    }
}
