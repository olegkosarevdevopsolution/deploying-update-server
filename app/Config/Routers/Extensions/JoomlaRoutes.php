<?php

namespace Config\Routers\Extensions;

use CodeIgniter\Router\RouteCollection;

class JoomlaRoutes
{
    protected RouteCollection $routes;

    public function __construct(RouteCollection $routes)
    {
        $this->routes = $routes;
    }


    public function getRoute()
    {
        $this->routes->group('joomla', ['namespace' => 'App\Controllers\Extensions\Joomla'], function ($joomlaRoutes) {

            // Set the routes for Joomla Templates
            $templatesRoute = new Joomla\TemplatesRoutes($this->routes);
            $templatesRoute->getRoutes();

            // Set the routes for Joomla Modules
            $modulesRoute = new Joomla\ModulesRoutes($this->routes);
            $modulesRoute->getRoutes();

            // Set the routes for Joomla Plugins
            $pluginsRoute = new Joomla\PluginsRoutes($this->routes);
            $pluginsRoute->getRoutes();

            // Set the routes for Joomla Components
            $componentsRoute = new Joomla\ComponentsRoutes($this->routes);
            $componentsRoute->getRoutes();

            // Set the routes for Joomla Languages
            $languagesRoute = new Joomla\LanguagesRoutes($this->routes);
            $languagesRoute->getRoutes();
        });
    }
}
