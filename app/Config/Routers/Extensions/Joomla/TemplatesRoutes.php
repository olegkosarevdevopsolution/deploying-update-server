<?php

namespace Config\Routers\Extensions\Joomla;

class TemplatesRoutes
{
    protected \CodeIgniter\Router\RouteCollection $routes;
    public function __construct(\CodeIgniter\Router\RouteCollection $routes)
    {
        $this->routes = $routes;
    }

    public function getRoutes()
    {
        // https: //extensions.test/extensions/joomla/templates/<templateName>.html - Информация для шаблона
        // https://extensions.test/extensions/joomla/templates/<templateName>/changelog.html - Ченжлог для шаблона
        $this->routes->get('templates/(:segment).html', 'Templates\TemplatesController::templates/$1');
        $this->routes->get('templates/(:segment)/changelog.html', 'Templates\ChangelogController::changelog/$1');
    }
}
