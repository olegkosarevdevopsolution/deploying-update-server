<?php

namespace Config\Routers\Extensions\Joomla;

class ModulesRoutes
{
    protected \CodeIgniter\Router\RouteCollection $routes;
    public function __construct(\CodeIgniter\Router\RouteCollection $routes)
    {
        $this->routes = $routes;
    }

    public function getRoutes()
    {
        // https: //extensions.test/extensions/joomla/modules/<templateName>.html - Информация для шаблона
        // https://extensions.test/extensions/joomla/modules/<templateName>/changelog.html - Ченжлог для шаблона
        $this->routes->get('modules/(:segment).html', 'Modules\ModulesController::modules/$1');
        $this->routes->get('modules/(:segment)/changelog.html', 'Modules\ChangelogController::changelog/$1');
    }
}
