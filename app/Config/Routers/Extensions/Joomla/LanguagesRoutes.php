<?php

namespace Config\Routers\Extensions\Joomla;

class LanguagesRoutes
{
    protected \CodeIgniter\Router\RouteCollection $routes;
    public function __construct(\CodeIgniter\Router\RouteCollection $routes)
    {
        $this->routes = $routes;
    }

    public function getRoutes()
    {
        // https: //extensions.test/extensions/joomla/languages/<templateName>.html - Информация для шаблона
        // https://extensions.test/extensions/joomla/languages/<templateName>/changelog.html - Ченжлог для шаблона
        $this->routes->get('languages/(:segment).html', 'Languages\LanguagesController::languages/$1');
        $this->routes->get('languages/(:segment)/changelog.html', 'Languages\ChangelogController::changelog/$1');
    }
}
