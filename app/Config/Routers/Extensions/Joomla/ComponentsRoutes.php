<?php

namespace Config\Routers\Extensions\Joomla;

class ComponentsRoutes
{
    protected \CodeIgniter\Router\RouteCollection $routes;
    public function __construct(\CodeIgniter\Router\RouteCollection $routes)
    {
        $this->routes = $routes;
    }

    public function getRoutes()
    {
        // https: //extensions.test/extensions/joomla/components/<templateName>.html - Информация для шаблона
        // https://extensions.test/extensions/joomla/components/<templateName>/changelog.html - Ченжлог для шаблона
        $this->routes->get('components/(:segment).html', 'Components\ComponentsController::components/$1');
        $this->routes->get('components/(:segment)/changelog.html', 'Components\ChangelogController::changelog/$1');
    }
}
