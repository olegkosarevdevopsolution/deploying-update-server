<?php

namespace Config\Routers\Update\Joomla;

class PluginsRoutes
{
    protected \CodeIgniter\Router\RouteCollection $routes;
    public function __construct(\CodeIgniter\Router\RouteCollection $routes)
    {
        $this->routes = $routes;
    }

    public function getRoutes()
    {
        // https: //extensions.test/extensions/joomla/plugins/<templateName>.html - Информация для шаблона
        // https://extensions.test/extensions/joomla/plugins/<templateName>/changelog.html - Ченжлог для шаблона
        $this->routes->get('plugins/(:segment).xml', 'Plugins\PluginsController::plugins/$1');
    }
}
