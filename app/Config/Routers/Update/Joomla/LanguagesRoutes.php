<?php

namespace Config\Routers\Update\Joomla;

class LanguagesRoutes
{
    protected \CodeIgniter\Router\RouteCollection $routes;
    public function __construct(\CodeIgniter\Router\RouteCollection $routes)
    {
        $this->routes = $routes;
    }

    public function getRoutes()
    {
        // https: //extensions.test/extensions/joomla/languages/<templateName>.html - Информация для шаблона
        // https://extensions.test/extensions/joomla/languages/<templateName>/changelog.html - Ченжлог для шаблона
        $this->routes->get('languages/(:segment).xml', 'Languages\LanguagesController::languages/$1');
    }
}
