<?php

namespace Config\Routers\Update\Joomla;

class ComponentsRoutes
{
    protected \CodeIgniter\Router\RouteCollection $routes;
    public function __construct(\CodeIgniter\Router\RouteCollection $routes)
    {
        $this->routes = $routes;
    }

    public function getRoutes()
    {
        // https: //extensions.test/extensions/joomla/components/<templateName>.html - Информация для шаблона
        // https://extensions.test/extensions/joomla/components/<templateName>/changelog.html - Ченжлог для шаблона
        $this->routes->get('components/(:segment).xml', 'Components\ComponentsController::components/$1');
    }
}
