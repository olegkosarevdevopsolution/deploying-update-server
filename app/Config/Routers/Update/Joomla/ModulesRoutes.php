<?php

namespace Config\Routers\Update\Joomla;

class ModulesRoutes
{
    protected \CodeIgniter\Router\RouteCollection $routes;
    public function __construct(\CodeIgniter\Router\RouteCollection $routes)
    {
        $this->routes = $routes;
    }

    public function getRoutes()
    {
        // https: //extensions.test/extensions/joomla/modules/<templateName>.html - Информация для шаблона
        // https://extensions.test/extensions/joomla/modules/<templateName>/changelog.html - Ченжлог для шаблона
        $this->routes->get('modules/(:segment).xml', 'Modules\ModulesController::modules/$1');
    }
}
