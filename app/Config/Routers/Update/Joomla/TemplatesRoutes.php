<?php

namespace Config\Routers\Update\Joomla;

class TemplatesRoutes
{
    protected \CodeIgniter\Router\RouteCollection $routes;
    public function __construct(\CodeIgniter\Router\RouteCollection $routes)
    {
        $this->routes = $routes;
    }

    public function getRoutes()
    {
        // https: //extensions.test/extensions/joomla/templates/<templateName>.html - Информация для шаблона
        // https://extensions.test/extensions/joomla/templates/<templateName>/changelog.html - Ченжлог для шаблона
        $this->routes->get('templates/(:segment).xml', 'Templates\TemplatesController::templates/$1');
    }
}
