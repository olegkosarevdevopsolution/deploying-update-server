<?php

namespace Config\Routers\Update;

use Config\Routers\Update\JoomlaRoutes;

class UpdateRoutes
{
    protected \CodeIgniter\Router\RouteCollection $routes;
    public function __construct(\CodeIgniter\Router\RouteCollection $routes)
    {
        $this->routes = $routes;
    }

    protected function getRouteCollection(): \CodeIgniter\Router\RouteCollection
    {
        return $this->routes;
    }

    public function getRoutes()
    {
        $this->routes->group('update', ['namespace' => 'App\Controllers\Update'], function ($extensionsRoutes) {

            $joomlaRoutes = new JoomlaRoutes($this->routes);
            $joomlaRoutes->getRoutes();
        });
    }
}
