<?php

use CodeIgniter\Router\RouteCollection;
use Config\Routers\Update\UpdateRoutes;
use Config\Routers\Extensions\ExtensionsRoutes;

/**
 * @var RouteCollection $routes
 */

// Route for the index page (landing page)
$routes->get('/', 'Home::index');

$routes->group('dashboard', ['namespace' => '\App\Dashboard\Controllers'], function ($dashboardRoutes) {
});


// $updateRoutes = new UpdateRoutes($routes);
// $updateRoutes->getRoutes();

// Nested group for extensions functionality
$extensionsRoutes = new ExtensionsRoutes($routes);
$extensionsRoutes->getRoutes();

$updateRoutes = new UpdateRoutes($routes);
$updateRoutes->getRoutes();
