<?php

namespace App\Interface;


/**
 * UpdateRouteHandlerInterface
 *
 * description
 *
 * @package \App\Interface;
 * @author Oleg Kosarev <dev.oleg.kosarev@outlook.com>
 * @copyright 2023 OlegKosarevDevOpsolution 
 * @license The MIT License (MIT)
 * @link https://dev.azure.com/OlegKosarevDevOpsolution/Deploying%20Update%20Server
 * @version 1.0.0
 * @since 1.0.0
 * 
 */

interface UpdateRouteHandlerInterface
{
    public function handle(string $name): string;
}
