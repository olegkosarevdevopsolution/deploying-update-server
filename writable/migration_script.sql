-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 31, 2024 at 05:32 AM
-- Server version: 8.0.30
-- PHP Version: 8.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `update`
--

-- --------------------------------------------------------

--
-- Table structure for table `ert_cscart4_addons_info`
--

CREATE TABLE `ert_cscart4_addons_info` (
  `id` int UNSIGNED NOT NULL,
  `element` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `from_version` varchar(255) NOT NULL,
  `to_version` varchar(255) NOT NULL,
  `timestamp` bigint NOT NULL,
  `size` bigint NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `ert_cscart4_download_links`
--

CREATE TABLE `ert_cscart4_download_links` (
  `id` int UNSIGNED NOT NULL,
  `addon_id` int UNSIGNED NOT NULL,
  `downloadurl` varchar(255) DEFAULT NULL,
  `format` varchar(255) NOT NULL DEFAULT 'zip'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `ert_jomla3_extension_mapping_supported_databases`
--

CREATE TABLE `ert_jomla3_extension_mapping_supported_databases` (
  `id` int UNSIGNED NOT NULL,
  `ext_id` int UNSIGNED NOT NULL,
  `sd_id` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `ert_jomla4_extension_mapping_supported_databases`
--

CREATE TABLE `ert_jomla4_extension_mapping_supported_databases` (
  `id` int UNSIGNED NOT NULL,
  `ext_id` int UNSIGNED NOT NULL,
  `sd_id` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `ert_jomla5_extension_mapping_supported_databases`
--

CREATE TABLE `ert_jomla5_extension_mapping_supported_databases` (
  `id` int UNSIGNED NOT NULL,
  `ext_id` int UNSIGNED NOT NULL,
  `sd_id` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `ert_joomla3_download_links`
--

CREATE TABLE `ert_joomla3_download_links` (
  `id` int UNSIGNED NOT NULL,
  `extension_id` int UNSIGNED NOT NULL,
  `downloadurl` varchar(255) DEFAULT NULL,
  `downloadsource` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'full',
  `format` varchar(255) NOT NULL DEFAULT 'zip'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `ert_joomla3_extensions_info`
--

CREATE TABLE `ert_joomla3_extensions_info` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `element` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `folder` varchar(255) DEFAULT NULL,
  `client` enum('0','1') NOT NULL DEFAULT '0',
  `version` varchar(255) NOT NULL,
  `infourl` varchar(255) DEFAULT NULL,
  `joomla_tag_name` varchar(255) DEFAULT NULL,
  `changelogurl` varchar(255) NOT NULL,
  `maintainer` varchar(255) NOT NULL,
  `maintainerurl` varchar(255) NOT NULL,
  `php_minimum_version` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `ert_joomla3_target_platforms`
--

CREATE TABLE `ert_joomla3_target_platforms` (
  `id` int UNSIGNED NOT NULL,
  `extension_id` int UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT 'joomla',
  `version` varchar(255) NOT NULL DEFAULT '.*',
  `min_dev_level` int DEFAULT NULL,
  `max_dev_level` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `ert_joomla4_download_links`
--

CREATE TABLE `ert_joomla4_download_links` (
  `id` int UNSIGNED NOT NULL,
  `extension_id` int UNSIGNED NOT NULL,
  `downloadurl` varchar(255) DEFAULT NULL,
  `downloadsource` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'full',
  `format` varchar(255) NOT NULL DEFAULT 'zip'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `ert_joomla4_extensions_info`
--

CREATE TABLE `ert_joomla4_extensions_info` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `element` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `folder` varchar(255) DEFAULT NULL,
  `client` enum('site','administrator') NOT NULL DEFAULT 'site',
  `version` varchar(255) NOT NULL,
  `infourl` varchar(255) DEFAULT NULL,
  `joomla_tag_name` varchar(255) DEFAULT NULL,
  `changelogurl` varchar(255) NOT NULL,
  `maintainer` varchar(255) NOT NULL,
  `maintainerurl` varchar(255) NOT NULL,
  `php_minimum_version` varchar(255) NOT NULL,
  `sha256` varchar(64) DEFAULT NULL,
  `sha384` varchar(96) DEFAULT NULL,
  `sha512` varchar(128) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `ert_joomla4_target_platforms`
--

CREATE TABLE `ert_joomla4_target_platforms` (
  `id` int UNSIGNED NOT NULL,
  `extension_id` int UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT 'joomla',
  `version` varchar(255) NOT NULL DEFAULT '.*',
  `min_dev_level` int DEFAULT NULL,
  `max_dev_level` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `ert_joomla5_download_links`
--

CREATE TABLE `ert_joomla5_download_links` (
  `id` int UNSIGNED NOT NULL,
  `extension_id` int UNSIGNED NOT NULL,
  `downloadurl` varchar(255) DEFAULT NULL,
  `downloadsource` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'full',
  `format` varchar(255) NOT NULL DEFAULT 'zip'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `ert_joomla5_extensions_info`
--

CREATE TABLE `ert_joomla5_extensions_info` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `element` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `folder` varchar(255) DEFAULT NULL,
  `client` enum('site','administrator') NOT NULL DEFAULT 'site',
  `version` varchar(255) NOT NULL,
  `infourl` varchar(255) DEFAULT NULL,
  `joomla_tag_name` varchar(255) DEFAULT NULL,
  `changelogurl` varchar(255) DEFAULT NULL,
  `maintainer` varchar(255) NOT NULL,
  `maintainerurl` varchar(255) NOT NULL,
  `php_minimum_version` varchar(255) NOT NULL,
  `sha256` varchar(64) DEFAULT NULL,
  `sha384` varchar(96) DEFAULT NULL,
  `sha512` varchar(128) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `ert_joomla5_target_platforms`
--

CREATE TABLE `ert_joomla5_target_platforms` (
  `id` int UNSIGNED NOT NULL,
  `extension_id` int UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT 'joomla',
  `version` varchar(255) NOT NULL DEFAULT '.*',
  `min_dev_level` int DEFAULT NULL,
  `max_dev_level` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Table structure for table `ert_joomla_tags`
--

CREATE TABLE `ert_joomla_tags` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `ert_joomla_tags`
--

INSERT INTO `ert_joomla_tags` (`id`, `name`) VALUES
(2, 'alpha'),
(3, 'beta'),
(1, 'dev'),
(4, 'rc'),
(5, 'stable');

-- --------------------------------------------------------

--
-- Table structure for table `ert_migrations`
--

CREATE TABLE `ert_migrations` (
  `id` bigint UNSIGNED NOT NULL,
  `version` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int NOT NULL,
  `batch` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `ert_migrations`
--

INSERT INTO `ert_migrations` (`id`, `version`, `class`, `group`, `namespace`, `time`, `batch`) VALUES
(115, '2024-01-11-000000', 'App\\Database\\Migrations\\SupportedDatabasesVersions', 'default', 'App', 1706664565, 1),
(116, '2024-01-11-000001', 'App\\Database\\Migrations\\JoomlaTags', 'default', 'App', 1706664566, 1),
(117, '2024-01-11-000002', 'App\\Database\\Migrations\\PhpVersions', 'default', 'App', 1706664566, 1),
(118, '2024-01-11-000003', 'App\\Database\\Migrations\\Joomla4\\Joomla4ExtensionsInfo', 'default', 'App', 1706664566, 1),
(119, '2024-01-11-000004', 'App\\Database\\Migrations\\Joomla4\\Joomla4ExtensionMappingSupportedDatabases', 'default', 'App', 1706664567, 1),
(120, '2024-01-11-000005', 'App\\Database\\Migrations\\Joomla4\\Joomla4DownloadsLinks', 'default', 'App', 1706664567, 1),
(121, '2024-01-11-000006', 'App\\Database\\Migrations\\Joomla4\\Joomla4TargetPlatforms', 'default', 'App', 1706664567, 1),
(122, '2024-01-11-000007', 'App\\Database\\Migrations\\Joomla5\\Joomla5ExtensionsInfo', 'default', 'App', 1706664568, 1),
(123, '2024-01-11-000008', 'App\\Database\\Migrations\\Joomla5\\Joomla5ExtensionMappingSupportedDatabases', 'default', 'App', 1706664568, 1),
(124, '2024-01-11-000009', 'App\\Database\\Migrations\\Joomla5\\Joomla5DownloadsLinks', 'default', 'App', 1706664568, 1),
(125, '2024-01-11-000010', 'App\\Database\\Migrations\\Joomla5\\Joomla5TargetPlatforms', 'default', 'App', 1706664569, 1),
(126, '2024-01-11-000011', 'App\\Database\\Migrations\\Joomla3\\Joomla3ExtensionsInfo', 'default', 'App', 1706664569, 1),
(127, '2024-01-11-000012', 'App\\Database\\Migrations\\Joomla3\\Joomla3ExtensionMappingSupportedDatabases', 'default', 'App', 1706664569, 1),
(128, '2024-01-11-000013', 'App\\Database\\Migrations\\Joomla3\\Joomla3DownloadsLinks', 'default', 'App', 1706664570, 1),
(129, '2024-01-11-000014', 'App\\Database\\Migrations\\Joomla3\\Joomla3TargetPlatforms', 'default', 'App', 1706664570, 1),
(130, '2024-01-12-000015', 'App\\Database\\Migrations\\CsCart4\\CsCart4AddonsInfo', 'default', 'App', 1706664570, 1),
(131, '2024-01-12-000016', 'App\\Database\\Migrations\\CsCart4\\CsCart4DownloadsLinks', 'default', 'App', 1706664570, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ert_php_versions`
--

CREATE TABLE `ert_php_versions` (
  `id` int UNSIGNED NOT NULL,
  `version` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `ert_php_versions`
--

INSERT INTO `ert_php_versions` (`id`, `version`) VALUES
(1, '5.1'),
(2, '5.2'),
(3, '5.3'),
(4, '5.4'),
(5, '5.5'),
(6, '5.6'),
(7, '7.0'),
(8, '7.1'),
(9, '7.2'),
(10, '7.3'),
(11, '7.4'),
(12, '8.0'),
(13, '8.1'),
(14, '8.2');

-- --------------------------------------------------------

--
-- Table structure for table `ert_supported_databases_versions`
--

CREATE TABLE `ert_supported_databases_versions` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `ert_supported_databases_versions`
--

INSERT INTO `ert_supported_databases_versions` (`id`, `name`, `version`) VALUES
(1, 'mariadb', '5.1'),
(2, 'mariadb', '5.2'),
(3, 'mariadb', '5.3'),
(4, 'mariadb', '5.5'),
(5, 'mariadb', '10.0'),
(6, 'mariadb', '10.1'),
(7, 'mariadb', '10.2'),
(8, 'mariadb', '10.3'),
(9, 'mariadb', '10.4'),
(10, 'mariadb', '10.5'),
(11, 'mariadb', '10.6'),
(12, 'mariadb', '10.7'),
(13, 'mariadb', '10.8'),
(14, 'mariadb', '10.9'),
(15, 'mariadb', '10.10'),
(16, 'mariadb', '10.11'),
(17, 'mariadb', '11.0'),
(18, 'mariadb', '11.1'),
(19, 'mariadb', '11.2'),
(20, 'mariadb', '11.3'),
(21, 'mariadb', '11.4'),
(22, 'mysql', '5.1'),
(23, 'mysql', '5.5'),
(24, 'mysql', '5.6'),
(25, 'mysql', '5.7'),
(26, 'mysql', '8.0'),
(27, 'mysql', '8.1'),
(28, 'mysql', '8.2'),
(29, 'postgresql', '6.0'),
(30, 'postgresql', '6.1'),
(31, 'postgresql', '6.2'),
(32, 'postgresql', '6.3'),
(33, 'postgresql', '6.4'),
(34, 'postgresql', '6.5'),
(35, 'postgresql', '7.0'),
(36, 'postgresql', '7.1'),
(37, 'postgresql', '7.2'),
(38, 'postgresql', '7.3'),
(39, 'postgresql', '7.4'),
(40, 'postgresql', '8.0'),
(41, 'postgresql', '8.1'),
(42, 'postgresql', '8.2'),
(43, 'postgresql', '8.3'),
(44, 'postgresql', '8.4'),
(45, 'postgresql', '9.0'),
(46, 'postgresql', '9.1'),
(47, 'postgresql', '9.2'),
(48, 'postgresql', '9.3'),
(49, 'postgresql', '9.4'),
(50, 'postgresql', '9.5'),
(51, 'postgresql', '9.6'),
(52, 'postgresql', '10	'),
(53, 'postgresql', '11	'),
(54, 'postgresql', '12	'),
(55, 'postgresql', '13	'),
(56, 'postgresql', '14	'),
(57, 'postgresql', '15	'),
(58, 'postgresql', '16'),
(59, 'mssql', '16.0.1000.6'),
(60, 'mssql', '15.0.2000.5'),
(61, 'mssql', '14.0.1000.169'),
(62, 'mssql', '13.0.1601.5'),
(63, 'mssql', '12.0.2000.8'),
(64, 'mssql', '11.0.2100.60'),
(65, 'mssql', '10.50.1600.1'),
(66, 'mssql', '10.0.1600.22'),
(67, 'mssql', '9.0.1399.06'),
(68, 'mssql', '8.0.194'),
(69, 'mssql', '7.0.623'),
(70, 'mssql', '6.00.121');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ert_cscart4_addons_info`
--
ALTER TABLE `ert_cscart4_addons_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ert_cscart4_download_links`
--
ALTER TABLE `ert_cscart4_download_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ert_cscart4_download_links_addon_id_foreign` (`addon_id`);

--
-- Indexes for table `ert_jomla3_extension_mapping_supported_databases`
--
ALTER TABLE `ert_jomla3_extension_mapping_supported_databases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ert_jomla3_extension_mapping_supported_databases_ext_id_foreign` (`ext_id`),
  ADD KEY `ert_jomla3_extension_mapping_supported_databases_sd_id_foreign` (`sd_id`);

--
-- Indexes for table `ert_jomla4_extension_mapping_supported_databases`
--
ALTER TABLE `ert_jomla4_extension_mapping_supported_databases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ert_jomla4_extension_mapping_supported_databases_ext_id_foreign` (`ext_id`),
  ADD KEY `ert_jomla4_extension_mapping_supported_databases_sd_id_foreign` (`sd_id`);

--
-- Indexes for table `ert_jomla5_extension_mapping_supported_databases`
--
ALTER TABLE `ert_jomla5_extension_mapping_supported_databases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ert_jomla5_extension_mapping_supported_databases_ext_id_foreign` (`ext_id`),
  ADD KEY `ert_jomla5_extension_mapping_supported_databases_sd_id_foreign` (`sd_id`);

--
-- Indexes for table `ert_joomla3_download_links`
--
ALTER TABLE `ert_joomla3_download_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ert_joomla3_download_links_extension_id_foreign` (`extension_id`);

--
-- Indexes for table `ert_joomla3_extensions_info`
--
ALTER TABLE `ert_joomla3_extensions_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ert_joomla3_extensions_info_php_minimum_version_foreign` (`php_minimum_version`),
  ADD KEY `ert_joomla3_extensions_info_joomla_tag_name_foreign` (`joomla_tag_name`);

--
-- Indexes for table `ert_joomla3_target_platforms`
--
ALTER TABLE `ert_joomla3_target_platforms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ert_joomla3_target_platforms_extension_id_foreign` (`extension_id`);

--
-- Indexes for table `ert_joomla4_download_links`
--
ALTER TABLE `ert_joomla4_download_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ert_joomla4_download_links_extension_id_foreign` (`extension_id`);

--
-- Indexes for table `ert_joomla4_extensions_info`
--
ALTER TABLE `ert_joomla4_extensions_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ert_joomla4_extensions_info_php_minimum_version_foreign` (`php_minimum_version`),
  ADD KEY `ert_joomla4_extensions_info_joomla_tag_name_foreign` (`joomla_tag_name`);

--
-- Indexes for table `ert_joomla4_target_platforms`
--
ALTER TABLE `ert_joomla4_target_platforms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ert_joomla4_target_platforms_extension_id_foreign` (`extension_id`);

--
-- Indexes for table `ert_joomla5_download_links`
--
ALTER TABLE `ert_joomla5_download_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ert_joomla5_download_links_extension_id_foreign` (`extension_id`);

--
-- Indexes for table `ert_joomla5_extensions_info`
--
ALTER TABLE `ert_joomla5_extensions_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ert_joomla5_extensions_info_php_minimum_version_foreign` (`php_minimum_version`),
  ADD KEY `ert_joomla5_extensions_info_joomla_tag_name_foreign` (`joomla_tag_name`);

--
-- Indexes for table `ert_joomla5_target_platforms`
--
ALTER TABLE `ert_joomla5_target_platforms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ert_joomla5_target_platforms_extension_id_foreign` (`extension_id`);

--
-- Indexes for table `ert_joomla_tags`
--
ALTER TABLE `ert_joomla_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `ert_migrations`
--
ALTER TABLE `ert_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ert_php_versions`
--
ALTER TABLE `ert_php_versions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `version` (`version`);

--
-- Indexes for table `ert_supported_databases_versions`
--
ALTER TABLE `ert_supported_databases_versions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ert_cscart4_addons_info`
--
ALTER TABLE `ert_cscart4_addons_info`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ert_cscart4_download_links`
--
ALTER TABLE `ert_cscart4_download_links`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ert_jomla3_extension_mapping_supported_databases`
--
ALTER TABLE `ert_jomla3_extension_mapping_supported_databases`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ert_jomla4_extension_mapping_supported_databases`
--
ALTER TABLE `ert_jomla4_extension_mapping_supported_databases`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ert_jomla5_extension_mapping_supported_databases`
--
ALTER TABLE `ert_jomla5_extension_mapping_supported_databases`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ert_joomla3_download_links`
--
ALTER TABLE `ert_joomla3_download_links`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ert_joomla3_extensions_info`
--
ALTER TABLE `ert_joomla3_extensions_info`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ert_joomla3_target_platforms`
--
ALTER TABLE `ert_joomla3_target_platforms`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ert_joomla4_download_links`
--
ALTER TABLE `ert_joomla4_download_links`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ert_joomla4_extensions_info`
--
ALTER TABLE `ert_joomla4_extensions_info`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ert_joomla4_target_platforms`
--
ALTER TABLE `ert_joomla4_target_platforms`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ert_joomla5_download_links`
--
ALTER TABLE `ert_joomla5_download_links`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ert_joomla5_extensions_info`
--
ALTER TABLE `ert_joomla5_extensions_info`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ert_joomla5_target_platforms`
--
ALTER TABLE `ert_joomla5_target_platforms`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ert_joomla_tags`
--
ALTER TABLE `ert_joomla_tags`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ert_migrations`
--
ALTER TABLE `ert_migrations`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `ert_php_versions`
--
ALTER TABLE `ert_php_versions`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `ert_supported_databases_versions`
--
ALTER TABLE `ert_supported_databases_versions`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ert_cscart4_download_links`
--
ALTER TABLE `ert_cscart4_download_links`
  ADD CONSTRAINT `ert_cscart4_download_links_addon_id_foreign` FOREIGN KEY (`addon_id`) REFERENCES `ert_cscart4_addons_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ert_jomla3_extension_mapping_supported_databases`
--
ALTER TABLE `ert_jomla3_extension_mapping_supported_databases`
  ADD CONSTRAINT `ert_jomla3_extension_mapping_supported_databases_ext_id_foreign` FOREIGN KEY (`ext_id`) REFERENCES `ert_joomla3_extensions_info` (`id`),
  ADD CONSTRAINT `ert_jomla3_extension_mapping_supported_databases_sd_id_foreign` FOREIGN KEY (`sd_id`) REFERENCES `ert_supported_databases_versions` (`id`);

--
-- Constraints for table `ert_jomla4_extension_mapping_supported_databases`
--
ALTER TABLE `ert_jomla4_extension_mapping_supported_databases`
  ADD CONSTRAINT `ert_jomla4_extension_mapping_supported_databases_ext_id_foreign` FOREIGN KEY (`ext_id`) REFERENCES `ert_joomla4_extensions_info` (`id`),
  ADD CONSTRAINT `ert_jomla4_extension_mapping_supported_databases_sd_id_foreign` FOREIGN KEY (`sd_id`) REFERENCES `ert_supported_databases_versions` (`id`);

--
-- Constraints for table `ert_jomla5_extension_mapping_supported_databases`
--
ALTER TABLE `ert_jomla5_extension_mapping_supported_databases`
  ADD CONSTRAINT `ert_jomla5_extension_mapping_supported_databases_ext_id_foreign` FOREIGN KEY (`ext_id`) REFERENCES `ert_joomla5_extensions_info` (`id`),
  ADD CONSTRAINT `ert_jomla5_extension_mapping_supported_databases_sd_id_foreign` FOREIGN KEY (`sd_id`) REFERENCES `ert_supported_databases_versions` (`id`);

--
-- Constraints for table `ert_joomla3_download_links`
--
ALTER TABLE `ert_joomla3_download_links`
  ADD CONSTRAINT `ert_joomla3_download_links_extension_id_foreign` FOREIGN KEY (`extension_id`) REFERENCES `ert_joomla3_extensions_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ert_joomla3_extensions_info`
--
ALTER TABLE `ert_joomla3_extensions_info`
  ADD CONSTRAINT `ert_joomla3_extensions_info_joomla_tag_name_foreign` FOREIGN KEY (`joomla_tag_name`) REFERENCES `ert_joomla_tags` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ert_joomla3_extensions_info_php_minimum_version_foreign` FOREIGN KEY (`php_minimum_version`) REFERENCES `ert_php_versions` (`version`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ert_joomla3_target_platforms`
--
ALTER TABLE `ert_joomla3_target_platforms`
  ADD CONSTRAINT `ert_joomla3_target_platforms_extension_id_foreign` FOREIGN KEY (`extension_id`) REFERENCES `ert_joomla3_extensions_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ert_joomla4_download_links`
--
ALTER TABLE `ert_joomla4_download_links`
  ADD CONSTRAINT `ert_joomla4_download_links_extension_id_foreign` FOREIGN KEY (`extension_id`) REFERENCES `ert_joomla4_extensions_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ert_joomla4_extensions_info`
--
ALTER TABLE `ert_joomla4_extensions_info`
  ADD CONSTRAINT `ert_joomla4_extensions_info_joomla_tag_name_foreign` FOREIGN KEY (`joomla_tag_name`) REFERENCES `ert_joomla_tags` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ert_joomla4_extensions_info_php_minimum_version_foreign` FOREIGN KEY (`php_minimum_version`) REFERENCES `ert_php_versions` (`version`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ert_joomla4_target_platforms`
--
ALTER TABLE `ert_joomla4_target_platforms`
  ADD CONSTRAINT `ert_joomla4_target_platforms_extension_id_foreign` FOREIGN KEY (`extension_id`) REFERENCES `ert_joomla4_extensions_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ert_joomla5_download_links`
--
ALTER TABLE `ert_joomla5_download_links`
  ADD CONSTRAINT `ert_joomla5_download_links_extension_id_foreign` FOREIGN KEY (`extension_id`) REFERENCES `ert_joomla5_extensions_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ert_joomla5_extensions_info`
--
ALTER TABLE `ert_joomla5_extensions_info`
  ADD CONSTRAINT `ert_joomla5_extensions_info_joomla_tag_name_foreign` FOREIGN KEY (`joomla_tag_name`) REFERENCES `ert_joomla_tags` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ert_joomla5_extensions_info_php_minimum_version_foreign` FOREIGN KEY (`php_minimum_version`) REFERENCES `ert_php_versions` (`version`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ert_joomla5_target_platforms`
--
ALTER TABLE `ert_joomla5_target_platforms`
  ADD CONSTRAINT `ert_joomla5_target_platforms_extension_id_foreign` FOREIGN KEY (`extension_id`) REFERENCES `ert_joomla5_extensions_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;